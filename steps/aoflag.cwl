#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: aoflagger

baseCommand:
  - aoflagger
inputs:
  - id: msin
    type: Directory[]
    doc: Input measurement set
    inputBinding:
      position: 1
      shellQuote: false
  - id: verbose
    type: boolean?
    doc: will produce verbose output
    inputBinding:
      position: 0
      valueFrom: -v
      shellQuote: false
  - id: n_threads
    type: int?
    doc: |
      overrides the number of threads specified in the strategy
      (default: one thread for each CPU core)
    inputBinding:
      prefix: -j
      position: 0
      shellQuote: false
  - id: strategy
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/rfistrategies/lofar-default.lua'
    doc: |
      specifies a customized strategy
    inputBinding:
      prefix: -strategy
      separate: true
      position: 0
      shellQuote: false

#### READ MODES
  - id: direct_read
    type: boolean?
    doc: |
      Will perform the slowest IO but will always work.
    inputBinding:
      prefix: -direct-read
      position: 0
      shellQuote: false
  - id: reorder
    type: boolean?
    doc: |
      Make aoflagger reorder the measurement set before running the detection.
      This prevents that aoflagger will use its memory reading mode, which is faster but uses more memory.
    inputBinding:
      prefix: -reorder
      position: 0
      shellQuote: false
  - id: memory_read
    type: boolean?
    doc: |
      Will read the entire measurement set in memory. This is the fastest,
      but requires much memory.
    inputBinding:
      prefix: -memory-read
      position: 0
      shellQuote: false
  - id: auto_read
    type: boolean?
    doc: |
       Will select either memory or direct mode based on available memory (default).
    inputBinding:
      prefix: -auto-read
      position: 0
      shellQuote: false
##############
  - id: skip_flagged
    type: boolean?
    doc: |
      Will skip an ms if it has already been processed by AOFlagger according
      to its HISTORY table.
    inputBinding:
      prefix: -skip-flagged
      position: 0
      shellQuote: false
  - id: uvw
    type: File?
    doc: Reads uvw values (some exotic strategies require these)
    inputBinding:
      prefix: -uvw
      position: 0
      shellQuote: false
  - id: column
    type: string?
    doc: Specify column to flag
    inputBinding:
      prefix: -column
      position: 0
      shellQuote: false
  - id: bands
    type: int[]?
    doc: Comma separated list of (zero-indexed) band ids to process
    inputBinding:
      prefix: -bands
      position: 0
      itemSeparator: ','
      shellQuote: false
  - id: fields
    type: int[]?
    doc: Comma separated list of (zero-indexed) field ids to process
    inputBinding:
      prefix: -fields
      itemSeparator: ','
      position: 0
      shellQuote: false
  - id: combine-spws
    type: boolean?
    doc: Join all SPWs together in frequency direction before flagging
    default: false
    inputBinding:
      prefix: -combine-spws
      position: 0
      shellQuote: false
  - id: concatenate-frequency
    type: boolean?
    doc: Concatenate all input SBs on-the-fly
    default: false
    inputBinding:
      prefix: -concatenate-frequency
      position: 0
      shellQuote: false
  - id: chunk-size
    type: int?
    doc:  This will split the set into intervals with the given maximum size, and flag each interval independently
    inputBinding:
      prefix: -chunk-size
      position: 0
      shellQuote: false

outputs:
  - id: output_ms
    doc: Output MS
    type: Directory[]
    outputBinding:
      outputEval: $(inputs.msin)
  - id: logfile
    type: File
    outputBinding:
      glob: aoflag.log
label: AOFlagger
requirements:
  - class: ShellCommandRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc

stdout: aoflag.log
stderr: aoflag_err.log
