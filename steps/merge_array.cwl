id: merge_array
label: merge_array
class: ExpressionTool

cwlVersion: v1.2
inputs:
    - id: input
      type:
        - type: array
          items:
            - type: array
              items: Directory
outputs:
    - id: output
      type: Directory[]

expression: |
  ${
    var out_dir = []
    for(var i=0; i<inputs.input.length; i++){
        var item = inputs.input[i]
        if(item != null){
            out_dir = out_dir.concat(item)
        }
    }
    return {'output': out_dir}
  }


requirements:
  - class: InlineJavascriptRequirement
