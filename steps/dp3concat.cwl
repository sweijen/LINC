class: CommandLineTool
cwlVersion: v1.2
id: dp3concat
baseCommand:
  - DP3
inputs:
  - id: msin
    type: Directory[]
    doc: Input Measurement Set
  - id: msin_fname
    doc: Input Measurement Set string (including dummy.ms)
    type: string[]
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
      itemSeparator: ','
      valueFrom: "[$(self.join(','))]"
  - id: msout_name
    type: string
    inputBinding:
      prefix: msout=
      separate: false
      shellQuote: false
      position: 0
  - id: msin_datacolumn
    type: string?
    inputBinding:
      prefix: msin.datacolumn=
      separate: false
      shellQuote: false
      position: 0
    doc: Input data Column
    default: DATA
  - id: msout_datacolumn
    type: string?
    inputBinding:
      prefix: msout.datacolumn=
      separate: false
      shellQuote: false
      position: 0
    default: DATA
  - id: filter_baselines
    type: string?
    inputBinding:
      prefix: filter.baseline=
      separate: false
      shellQuote: true
      position: 0
      valueFrom: $(self)
    default: null
  - id: filter_remove
    type: boolean?
    inputBinding:
      prefix: filter.remove=True
      shellQuote: false
      position: 0
    default: false
  - id: writefullresflag
    type: boolean?
    inputBinding:
      prefix: msout.writefullresflag=True
      shellQuote: false
      position: 0
    default: false
  - id: overwrite
    type: boolean?
    inputBinding:
      prefix: msout.overwrite=True
      shellQuote: false
      position: 0
    default: false
  - id: storagemanager
    type: string?
    inputBinding:
      prefix: msout.storagemanager=
      separate: false
      shellQuote: false
      position: 0
    default: ''
  - id: databitrate
    type: int?
    inputBinding:
      prefix: msout.storagemanager.databitrate=
      separate: false
      shellQuote: false
      position: 0
  - id: missingdata
    type: boolean?
    inputBinding:
      prefix: msin.missingdata=True
      separate: false
      shellQuote: false
      position: 0
  - id: baseline
    type: string?
    inputBinding:
      prefix: msin.baseline=
      separate: false
      shellQuote: true
      position: 0
    default: '*'
  - id: avg_timeresolution
    type: int?
    inputBinding:
      prefix: avg.timeresolution=
      separate: false
      shellQuote: false
      position: 0
    default: 1
  - id: avg_freqresolution
    type: string?
    inputBinding:
      prefix: avg.freqresolution=
      separate: false
      shellQuote: false
      position: 0
    default: 12.205kHz
  - id: save2json
    default: true
    type: boolean?
    inputBinding:
      position: 0
      prefix: count.savetojson=True
  - id: jsonfilename
    type: string?
    default: 'out.json'
    inputBinding:
      prefix: count.jsonfilename=
      separate: false
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msout_name)
  - id: flagged_fraction_dict
    type: string
    outputBinding:
        loadContents: true
        glob: $(inputs.jsonfilename)
        outputEval: $(JSON.parse(self[0].contents).flagged_fraction_dict)
  - id: logfile
    type: File[]
    outputBinding:
      glob: concat*.log
arguments:
  - steps=[filter,avg,count]
  - msin.orderms=False
  - avg.type=average
requirements:
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: 8
hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: false
stdout: concat.log
stderr: concat_err.log
