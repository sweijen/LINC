id: check_filtered_MS_array
label: check_filtered_MS_array
class: ExpressionTool

cwlVersion: v1.2
inputs:
    - id: input
      type: Directory[]

outputs:
    - id: output
      type: Directory[]

expression: |
  ${
    if (inputs.input.length == 0) {
        throw new Error("No MS files meet the minimum unflagged fraction set by min_unflagged_fraction")
    } else {
        return {'output': inputs.input}
    }
  }


requirements:
  - class: InlineJavascriptRequirement
