#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: losoto_flagstation

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  This operation for LoSoTo implements a station-flagging procedure. Flags are time-independent.
  WEIGHT: compliant

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('FLAGSTATION').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - '--verbose'
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: mode
    type:
      type: enum
      symbols:
        - bandpass
        - resid
    doc: |
      Fitting algorithm: bandpass or resid. Bandpass mode clips amplitudes
       relative to a model bandpass (only LOFAR is currently supported).
      Resid mode clips residual phases or log(amplitudes).
  - id: maxFlaggedFraction
    type: float?
    doc: |
      This sets the maximum allowable fraction of flagged solutions above
      which the entire station is flagged.
  - id: nSigma
    type: float?
    doc: |
      This sets the number of standard deviations considered when outlier
       clipping is done.
  - id: maxStddev
    type: float?
    doc: |
      Maximum allowable standard deviation when outlier clipping is done. For phases, this should value
      should be in radians, for amplitudes in log(amp). If None (or negative), a value of 0.1 rad is
      used for phases and 0.01 for amplitudes.
  - id: ampRange
    type: float[]?
    doc: |
      2-element array of the median amplitude level to be acceptable,
       ampRange[0]: lower limit, ampRange[1]: upper limit.
      If None or [0, 0], a reasonable range for typical observations is used.
  - id: telescope
    type: string?
    doc: Specifies the telescope if mode = 'bandpass'.
  - id: skipInternational
    type: boolean?
    doc: |
      If True, skip flagging of international LOFAR stations (only used if
       telescope = 'lofar')
  - id: refAnt
    type: string?
    doc: |
      If mode = resid, this sets the reference antenna for phase solutions,
       by default None.
  - id: soltabExport
    type: string?
    doc: |
      Soltab to export station flags to. Note: exported flags are not time- or
      frequency-dependent.
  - id: ncpu
    type: int?
    doc: Number of cpu to use, by default all available.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
