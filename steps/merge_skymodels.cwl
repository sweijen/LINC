class: CommandLineTool
cwlVersion: v1.2
id: merge_skymodels
baseCommand:
  - merge_skymodels.py
inputs:
  - id: inmodel1
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/skymodels/Ateam_LBA_CC.skymodel'
    inputBinding:
      prefix: --inmodel1=
      separate: false
      shellQuote: false
  - id: inmodel2
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/skymodels/Ateam_LBA_CC.skymodel'
    inputBinding:
      prefix: --inmodel2=
      separate: false
      shellQuote: false
  - id: outmodel
    type: string?
    default: 'output.skymodel'
    inputBinding:
      prefix: --outmodel=
      separate: false
      shellQuote: false
outputs:
  - id: skymodel_out
    type: File
    outputBinding:
      glob: $(inputs.outmodel)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'merge_skymodels*.log'
label: merge_skymodels
hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
stdout: merge_skymodels.log
stderr: merge_skymodels_err.log
