class: CommandLineTool
cwlVersion: v1.2
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
id: h5parm_cat
baseCommand:
  - H5parm_collector.py
inputs:
  - format: 'lofar:#H5Parm'
    id: h5parmFile
    type: File
    inputBinding:
      position: 0
    doc: List of h5parm files
  - default: sol000
    id: insolset
    type: string?
    inputBinding:
      position: 0
      prefix: '--insolset'
    doc: Input solset name
  - default: sol000
    id: outsolset
    type: string?
    inputBinding:
      position: 0
      prefix: '--outsolset'
    doc: Output solset name
  - id: insoltab
    type: string?
    inputBinding:
      position: 0
      prefix: '--insoltab'
    doc: Output solset name
  - default: output.h5
    id: input_file
    type: File
    doc: Output h5parm name
  - default: false
    id: squeeze
    type: boolean
    inputBinding:
      position: 0
      prefix: '-q'
    doc: removes all axes with the length of 1
  - default: true
    id: verbose
    type: boolean
    inputBinding:
      position: 0
      prefix: '-v'
    doc: verbose output
  - default: false
    id: clobber
    type: boolean
    inputBinding:
      position: 0
      prefix: '-c'
    doc: overwrite output
  - default: false
    id: history
    type: boolean
    inputBinding:
      position: 0
      prefix: '-H'
    doc: Keep history of the export soltabs
outputs:
  - id: outh5parm
    doc: Output h5parm
    type: File
    outputBinding:
      glob: $(inputs.input_file.basename)
    format: lofar:#H5Parm
  - id: log
    type: File[]
    outputBinding:
      glob: 'h5parm_collector_output*.log'
label: h5parm_cat
arguments:
  - position: 0
    prefix: '-o'
    valueFrom: $(inputs.input_file.basename)
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.input_file)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: h5parm_collector_output.log
stderr: h5parm_collector_output_err.log
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
