class: CommandLineTool
cwlVersion: v1.2
id: find_skymodel_cal_py
baseCommand:
  - python3
inputs:
    - id: msin
      type:
       - Directory
       - Directory[]
      doc: MS containing the calibrator
      inputBinding:
        position: 0
    - id: skymodels
      type:
        - Directory?
        - File?
      doc: Directory or file containing the sky models
    - id: skymodels_extension
      type: string?
      doc: path extension of the sky models
    - id: max_separation_arcmin
      type: float?
      doc: max separation in arc minutes

label: find_skymodel_cal.py
arguments:
  - find_sky.py
outputs:
  - id: output_models
    type: File
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).file)

  - id: model_name
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).skymodel_name)
  - id: logfile
    type: File
    outputBinding:
      glob: find_skymodel_cal.log

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: 'find_sky.py'
        entry: |
            import sys
            import os
            import shutil
            import json

            from unittest.mock import MagicMock
            sys.modules['lofarpipe.support.data_map'] = MagicMock()


            from find_skymodel_cal import main as find_skymodel

            mss = sys.argv[1:]
            inputs = json.loads(r"""$(inputs)""")

            skymodels = inputs['skymodels']
            max_separation_arcmin = inputs['max_separation_arcmin']
            max_separation_arcmin = 1.0 if max_separation_arcmin is None else max_separation_arcmin
            extension = inputs['skymodels_extension']
            if skymodels is None:
                skymodels = os.path.expandvars("$LINC_DATA_ROOT/skymodels")
            else:
                skymodels = skymodels["path"]
            output = {}
            if extension is not None:
                output = find_skymodel(mss, skymodels, extension, max_separation_arcmin=max_separation_arcmin)
            else:
                output = find_skymodel(mss, skymodels, max_separation_arcmin=max_separation_arcmin)

            skymodel_path = output['SkymodelCal']
            skymodel_name = output['SkymodelName']
            skymodel_path = shutil.copy(skymodel_path, os.getcwd())
            cwl_output = {"file":{'class': 'File', 'path': skymodel_path},
                          "skymodel_name": skymodel_name}

            with open('./out.json', 'w') as fp:
                json.dump(cwl_output, fp)


hints:
  DockerRequirement:
    dockerPull: astronrd/linc
stdout: find_skymodel_cal.log
stderr: find_skymodel_cal_err.log
