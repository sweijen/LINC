class: CommandLineTool
cwlVersion: v1.2
id: createRMh5parm
label: createRMh5parm
baseCommand:
  - createRMh5parm.py
inputs:
  - id: msin
    type:
      - Directory
      - type: array
        items: Directory
    inputBinding:
      position: 1
    doc: Input measurement set
  - id: h5parm
    type: File
    inputBinding:
      position: 2
    doc: Input h5parm solutions file
  - id: ionex_server
    type: string?
    doc: IONEX Server path
    default: 'http://ftp.aiub.unibe.ch/CODE/'
    inputBinding:
      prefix: '--server'
      position: 0
  - id: ionex_prefix
    type: string?
    doc: IONEX Prefix
    default: 'CODG'
    inputBinding:
      prefix: '--prefix'
      position: 0
  - id: ionex_path
    type: string?
    doc: IONEX path
    default: './'
    inputBinding:
      prefix: '--path'
      position: 0
  - id: solset
    type: string?
    doc: solset in which IONEX solution are put
    default: 'sol000'
    inputBinding:
      prefix: '--solsetName'
      position: 0
  - id: timestep
    type: float?
    doc: timestep in seconds
    default: 300.
    inputBinding:
      prefix: '-t'
      position: 0
  - id: smart_interpol
    type: float?
    doc: float parameter describing how much of Earth rotation is taken in to account in interpolation of the IONEX files. 1.0 means time interpolation assumes ionosphere rotates in opposite direction of the Earth
    default: 0.
    inputBinding:
      prefix: '-e'
      position: 0
  - id: proxyserver
    type: string?
    doc: Specify proxy server if necessary
    default: null
    inputBinding:
      prefix: '--proxyserver'
      position: 0
  - id: proxyport
    type: int?
    doc: Specify proxy port if necessary
    default: null
    inputBinding:
      prefix: '--proxyport'
      position: 0
  - id: proxytype
    type: string?
    doc: Specify proxy type if necessary
    default: null
    inputBinding:
      prefix: '--proxytype'
      position: 0
  - id: proxyuser
    type: string?
    doc: Specify proxy user name if necessary
    default: null
    inputBinding:
      prefix: '--proxyuser'
      position: 0
  - id: proxypass
    type: string?
    doc: Specify proxy server password if necessary
    default: null
    inputBinding:
      prefix: '--proxypass'
      position: 0
outputs:
  - id: h5parmout
    doc: h5parm output
    type: File
    format: 'lofar:#H5Parm'
    outputBinding:
      glob: $(inputs.h5parm.basename)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'createh5parm*.log'
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
 - class: InitialWorkDirRequirement
   listing:
     - entry: $(inputs.h5parm)
       writable: true
requirements:
  - class: NetworkAccess
    networkAccess: true
stdout: createh5parm.log
stderr: createh5parm_err.log
$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
