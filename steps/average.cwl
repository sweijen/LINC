class: CommandLineTool
cwlVersion: v1.2
id: average
baseCommand:
  - DP3
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - id: msout_name
    type: string
    inputBinding:
      prefix: msout=
      separate: false
      shellQuote: false
      position: 0
  - id: msin_datacolumn
    type: string
    inputBinding:
      prefix: msin.datacolumn=
      separate: false
      shellQuote: false
      position: 0
    doc: Input data Column
    default: DATA
  - id: writefullresflag
    type: boolean
    inputBinding:
      prefix: msout.writefullresflag=True
      shellQuote: false
      position: 0
    default: false
  - id: overwrite
    type: boolean
    inputBinding:
      prefix: msout.overwrite=True
      shellQuote: false
      position: 0
    default: false
  - id: storagemanager
    type: string
    inputBinding:
      prefix: msout.storagemanager=
      separate: false
      shellQuote: false
      position: 0
    default: ''
  - id: databitrate
    type: int?
    inputBinding:
      prefix: msout.storagemanager.databitrate=
      separate: false
      shellQuote: false
      position: 0
  - id: avg_timestep
    type: int?
    inputBinding:
      prefix: avg.timestep=
      separate: false
      shellQuote: false
      position: 0
    default: 1
  - id: avg_freqstep
    type: int?
    inputBinding:
      prefix: avg.freqstep=
      separate: false
      shellQuote: false
      position: 0
    default: 1
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msout_name)
  - id: logfile
    type: File[]
    outputBinding:
      glob: average*.log
arguments:
  - 'steps=[avg]'
  - msin.orderms=False
  - avg.type=average
requirements:
  - class: ShellCommandRequirement
  - class: ResourceRequirement
    coresMin: 8
hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
stdout: average.log
stderr: average_err.log
