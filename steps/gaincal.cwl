class: CommandLineTool
cwlVersion: v1.2
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
id: gaincal
baseCommand:
  - DP3
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - id: caltype
    type:
      type: enum
      symbols:
        - diagonal
        - fulljones
        - phaseonly
        - scalarphase
        - amplitude
        - scalaramplitude
        - tec
        - tecandphase
      name: caltype
    inputBinding:
      position: 0
      prefix: gaincal.caltype=
      separate: false
    doc: |
      The type of calibration that needs to be performed.
  - id: sourcedb
    type:
      - File
      - Directory
    inputBinding:
      position: 0
      prefix: gaincal.sourcedb=
      separate: false
  - default: true
    id: usebeammodel
    type: boolean?
    inputBinding:
      position: 0
      prefix: gaincal.usebeammodel=true
      separate: false
  - default: 1
    id: solint
    type: int?
    doc: >
      Number of time slots on which a solution is assumed to be constant (same
      as CellSize.Time in BBS).

      0 means all time slots. Note that for larger settings of solint, and
      specially for solint = 0,

      the memory usage of gaincal will be large (all visibilities for a solint
      should fit in memory).
  - default: DATA
    id: msin_datacolumn
    type: string?
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - default: false
    id: onebeamperpatch
    type: boolean?
    inputBinding:
      position: 0
      prefix: gaincal.onebeamperpatch=True
      separate: false
    doc: Input data Column
  - default: MODEL_DATA
    id: msin_modelcolum
    type: string?
    inputBinding:
      position: 0
      prefix: msin.modelcolumn=
      separate: false
    doc: Model data Column
  - default: instrument.h5
    id: output_name_h5parm
    type: string?
    inputBinding:
      position: 0
      prefix: gaincal.parmdb=
      separate: false
  - default: '.'
    id: msout_name
    type: string?
    inputBinding:
      position: 0
      prefix: msout=
      separate: false
    doc: Output Measurement Set
  - default: true
    id: propagatesolutions
    type: boolean?
    inputBinding:
      position: 0
      prefix: gaincal.propagatesolutions=True
      separate: false
  - default: true
    id: usechannelfreq
    type: boolean?
    inputBinding:
      position: 0
      prefix: gaincal.usechannelfreq=True
      separate: false
  - default: array_factor
    id: beammode
    type: string?
    inputBinding:
      position: 0
      prefix: gaincal.beammode=
      separate: false
  - default: 0
    id: nchan
    type: int?
    inputBinding:
      position: 0
      prefix: gaincal.nchan=
      separate: false
  - default: 50
    id: maxiter
    type: int?
    inputBinding:
      position: 0
      prefix: gaincal.maxiter=
      separate: false
  - default: 1e-3
    id: tolerance
    type: float?
    inputBinding:
      position: 0
      prefix: gaincal.tolerance=
      separate: false
  - default: null
    id: blrange
    type: int[]?
    inputBinding:
      position: 0
      prefix: filter.blrange=
      separate: false
      itemSeparator: ','
      valueFrom: "[$(self.join(','))]"
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: '$(inputs.msout_name=="."?inputs.msin.basename:inputs.msout_name)'
  - id: h5parm
    doc: Filename of output H5Parm (to be read by e.g. losoto)
    type: File
    outputBinding:
      glob: $(inputs.output_name_h5parm)
    format: 'lofar:#H5Parm'
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'gaincal*.log'
stdout: gaincal.log
stderr: gaincal_err.log
arguments:
  - 'steps=[filter,gaincal,count]'
hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: 2
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
