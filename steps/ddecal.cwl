#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: calib_rot_diag
baseCommand: DP3

arguments:
  - steps=[ddecal,count]

inputs:
  - id: msin
    type: Directory
    doc: Input Measurement Set
    inputBinding:
      prefix: msin=
      separate: false

  - id: msin_datacolumn
    type: string?
    default: DATA
    doc: Input data Column
    inputBinding:
      prefix: msin.datacolumn=
      separate: false

  - id: output_name_h5parm
    type: string?
    default: instrument.h5
    inputBinding:
      prefix: ddecal.h5parm=
      separate: false
  - id: msout_name
    type: string?
    doc: Output Measurement Set
    default: '.'
    inputBinding:
      prefix: msout=
      separate: false

#--------------------
  - id: propagate_solutions
    type: boolean?
    default: true
    inputBinding:
      prefix: ddecal.propagatesolutions=True
  - id: propagate_converged_only
    type: boolean?
    default: true
    inputBinding:
      prefix: ddecal.propagateconvergedonly=True
  - id: modeldatacolumns
    type: string[]?
    default: []
    inputBinding:
      prefix: ddecal.modeldatacolumns=
      separate: false
      valueFrom: "[$(self.join(','))]"
      shellQuote: false
  - id: flagunconverged
    type: boolean?
    default: false
    doc: |
      Flag unconverged solutions (i.e., those from solves that did not converge
      within maxiter iterations).
    inputBinding:
      prefix: ddecal.flagunconverged=True
  - id: flagdivergedonly
    default: true
    type: boolean?
    doc: |
      Flag only the unconverged solutions for which divergence was detected.
      At the moment, this option is effective only for rotation+diagonal
      solves, where divergence is detected when the amplitudes of any station
      are found to be more than a factor of 5 from the mean amplitude over all
      stations.
      If divergence for any one station is detected, all stations are flagged
      for that solution interval. Only effective when flagunconverged=true
      and mode=rotation+diagonal.
    inputBinding:
      prefix: ddecal.flagdivergedonly=True
  - id: storagemanager
    type: string?
    default: ""
    inputBinding:
      prefix: msout.storagemanager=
      separate: false
  - id: mode
    type:
      type: enum
      symbols:
        - scalarcomplexgain
        - scalarphase
        - scalaramplitude
        - tec
        - tecandphase
        - fulljones
        - diagonal
        - phaseonly
        - amplitudeonly
        - rotation
        - rotation+diagonal
    doc: |
      Type of constraint to apply. Options are
    inputBinding:
      prefix: ddecal.mode=
      separate: false
  - id: nchan
    type: int?
    default: 1
    inputBinding:
      position: 0
      prefix: ddecal.nchan=
      separate: false
  - id: maxiter
    type: int?
    default: 50
    inputBinding:
      position: 0
      prefix: ddecal.maxiter=
      separate: false
  - id: tolerance
    type: float?
    default: 1e-3
    inputBinding:
      position: 0
      prefix: ddecal.tolerance=
      separate: false
  - id: uvlambdamin
    type: int?
    default: 300
    inputBinding:
      position: 0
      prefix: ddecal.uvlambdamin=
      separate: false
  - id: solint
    type: int?
    default: 1
    inputBinding:
      position: 0
      prefix: ddecal.solint=
      separate: false

#--------------------
  - id: save2json
    default: true
    type: boolean?
    inputBinding:
      position: 0
      prefix: count.savetojson=True
  - id: jsonfilename
    type: string?
    default: 'out.json'
    inputBinding:
      prefix: count.jsonfilename=
      separate: false

outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)

  - id: h5parm
    doc: Filename of output H5Parm (to be read by e.g. losoto)
    type: File
    format: 'lofar:#H5Parm'
    outputBinding:
      glob: $(inputs.output_name_h5parm)
      
  - id: flagged_fraction_dict
    type: string
    outputBinding:
        loadContents: true
        glob: $(inputs.jsonfilename)
        outputEval: $(JSON.parse(self[0].contents).flagged_fraction_dict)

  - id: logfile
    type: File[]
    outputBinding:
      glob: 'ddecal*.log'

hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: 2

stdout: ddecal.log
stderr: ddecal_err.log

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
