class: CommandLineTool
cwlVersion: v1.2
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
id: get_targetname
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory[]
      doc: MS to compare with
      inputBinding:
        position: 0

arguments:
  - '-c'
  - |
    import sys
    import json
    from getTargetName import main as getTargetName
    
    mss = sys.argv[1:]

    output = getTargetName(mss)

    targetName = output['targetName']
    cwl_output = {"targetName": targetName}

    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)
        
outputs:
  - id: targetname
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).targetName)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'get_targetname*.log'
        
requirements:
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: get_targetname.log
stderr: get_targetname_err.log
