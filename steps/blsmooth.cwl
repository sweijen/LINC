class: CommandLineTool
cwlVersion: v1.2
id: blsmooth
label: BLsmooth
baseCommand:
  - BLsmooth.py
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 1
    doc: Input measurement set
  - default: 0.2
    id: ionfactor
    type: float?
    inputBinding:
      position: 0
      prefix: '-f'
    doc: Gives an indication on how strong is the ionosphere
  - id: do_smooth
    type: boolean?
    default: true
    doc: 'If true performs smoothing'
    inputBinding:
      prefix: -S
      valueFrom: "$(self ? 'True': 'False')"
      separate: true
  - default: 0.5
    id: bscalefactor
    type: float?
    inputBinding:
      position: 0
      prefix: '-s'
    doc: Gives an indication on how the smoothing varies with
  - default: DATA
    id: in_column_name
    type: string?
    inputBinding:
      position: 0
      prefix: '-i'
    doc: Column name to smooth
  - default: SMOOTHED_DATA
    id: out_column
    type: string?
    inputBinding:
      position: 0
      prefix: '-o'
    doc: Output column
  - default: false
    id: weight
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-w'
    doc: >-
      Save the newly computed WEIGHT_SPECTRUM, this action permanently modify
      the MS!
  - default: false
    id: restore
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-r'
    doc: If WEIGHT_SPECTRUM_ORIG exists then restore it before smoothing
  - default: false
    id: nobackup
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-b'
    doc: Do not backup the old WEIGHT_SPECTRUM in WEIGHT_SPECTRUM_ORIG
  - default: false
    id: onlyamp
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-a'
    doc: Smooth only amplitudes
outputs:
  - id: msout
    doc: MS set output
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    type: File
    outputBinding:
      glob: BLsmooth.log
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
requirements:
 - class: InitialWorkDirRequirement
   listing:
    - entry: $(inputs.msin)
      writable: true
 - class: InplaceUpdateRequirement
   inplaceUpdate: true 
 - class: InlineJavascriptRequirement
stderr: BLsmooth.log
