id: fetchfile
label: fetch_file
class: CommandLineTool
cwlVersion: v1.2
inputs: 
  - id: surl_link
    type: string
    inputBinding:
      position: 0
    
outputs: 
  - id: downloaded
    type: File
    outputBinding:
      glob: 'out/*'
baseCommand: 
 - 'bash'
 - 'fetch.sh'
doc: 'Fetch a file from surl'
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'fetch.sh' 
        entry: |
          #!/bin/bash
          mkdir out
          cd out
          turl=`echo $1 | awk '{gsub("srm://srm.grid.sara.nl[:0-9]*","gsiftp://gridftp.grid.sara.nl"); print}'`
          filename=`echo $1 | awk '{sub(".*/", "", $1); print $0}'`
          echo "Downloading $turl"
          globus-url-copy $turl file://$PWD/$filename

    
