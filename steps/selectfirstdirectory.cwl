id: selectfirstdirectory
label: selectfirstdirectory
class: ExpressionTool

cwlVersion: v1.2
inputs: 
    - id: input
      type: Directory[]
outputs: 
    - id: output
      type: Directory

requirements:

    - class: InlineJavascriptRequirement
    - class: InitialWorkDirRequirement
      listing:
        - $(inputs.input)
      

expression: |
  ${
    return {'output': inputs.input[0]}
  }
