#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: losoto_residual

doc: Subtract/divide two tables or a clock/tec/tec3rd/rm from a phase.

requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: |
          [residuals]
          soltab = $(inputs.soltab)
          operation=RESIDUALS
          soltabsToSub=$(inputs.soltabsToSub)
          ratio=$(inputs.ratio)

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - '--verbose'
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: soltabsToSub
    type: string[]
    doc: List of soltabs to subtract
  - id: ratio
    type: boolean?
    default: false
    doc: Return the ratio instead of subtracting.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
