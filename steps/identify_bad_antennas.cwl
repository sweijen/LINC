class: CommandLineTool
cwlVersion: v1.2
id: identify_bad_antennas
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory
      doc: MS to compare with
      inputBinding:
        position: 0

label: identifyBadAntennas.py
arguments:
  - '-c'
  - |
    import sys
    import json
    from identifyBadAntennas import main as identifyBadAntennas
    
    ms = sys.argv[1]

    output = identifyBadAntennas(ms)

    flaggedants = output['flaggedants']
    cwl_output  = {"flaggedants": flaggedants}

    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)
        
outputs:
  - id: flaggedants
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).flaggedants)
  - id: logfile
    type: File
    outputBinding:
      glob: identifyBadAntennas.log    
        
requirements:
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: identifyBadAntennas.log
stderr: identifyBadAntennas_err.log