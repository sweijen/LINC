class: CommandLineTool
cwlVersion: v1.2
id: identify_bad_antennas_join
baseCommand:
  - python3
  - indentify_bad_antennas.py
inputs:
    - id: flaggedants
      type: string[]?
      default: []
      doc: list of flagged antennas
    - id: filter
      type: string?
      default: '*&'
      doc: Filter these baselines for the comparison
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: indentify_bad_antennas.py
        entry: |
          import sys
          import json

          inputs = json.loads(r"""$(inputs)""")
          flaggedants = inputs['flaggedants']
          filter = inputs['filter']


          flaggedants_list = [ flaggedant.split(',') for flaggedant in flaggedants ]
          if flaggedants_list:
              flagged_antenna_list = set.intersection(*map(set, flaggedants_list))
          else:
              flagged_antenna_list = set()

          for flagged_antenna in flagged_antenna_list:
              if flagged_antenna != '':
                  filter += ';!' + flagged_antenna + '*&&*'

          cwl_output  = {"filter": filter}

          with open('./out.json', 'w') as fp:
              json.dump(cwl_output, fp)

outputs:
  - id: filter_out
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).filter)
  - id: logfile
    type: File
    outputBinding:
      glob: identifyBadAntennas.log
stdout: identifyBadAntennas.log
stderr: identifyBadAntennas_err.log
