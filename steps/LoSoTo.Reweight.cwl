#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: losoto_reweight

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  Change the the weight values.
  Parameters


requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('REWEIGHT').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - '--verbose'
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: mode
    default: 'uniform'
    type:
      - type: enum
        symbols:
          - uniform
          - window
          - copy
    doc: One of 'uniform' (single value), 'window' (sliding window in time), or 'copy' (copy from another table), by default 'uniform'.
  - id: weightVal
    type: float?
    doc: Set weights to this values (0=flagged), by default 1.
  - id: nmedian
    type: int?
    doc: |
      Median window size in number of timeslots for 'window' mode.
      If nonzero, a median-smoothed version of the input values is
      subtracted to detrend them. If 0, no smoothing or subtraction is
      done, by default 3.
  - id: nstddev
    type: int?
    doc: Standard deviation window size in number of timeslots for 'window' mode, by default 251.
  - id: soltabImport
    type: string?
    doc: Name of a soltab. Copy weights from this soltab (must have same axes shape), by default none.
  - id: flagBad
    type: boolean?
    doc: Re-apply flags to bad values (1 for amp, 0 for other tables), by default False.


outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
