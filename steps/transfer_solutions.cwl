class: CommandLineTool
cwlVersion: v1.2
id: transfer_solutions
baseCommand:
  - transfer_solutions.py
inputs:
  - default: output.h5
    id: h5parm
    type: File
    inputBinding:
      position: 1
    doc: H5parm to which the solutions should be transferred.
  - id: refh5parm
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/solutions/3C48.h5'
    inputBinding:
      position: 0
      prefix: '--refh5parm'
    doc: Name of the h5parm from which the solutions should be transferred.
  - id: insolset
    type: string?
    inputBinding:
      position: 0
      prefix: '--insolset'
    doc: Name of the input h5parm solution set
  - id: outsolset
    type: string?
    inputBinding:
      position: 0
      prefix: '--outsolset'
    doc: Name of the output h5parm solution set
  - id: insoltab
    type: string?
    inputBinding:
      position: 0
      prefix: '--insoltab'
    doc: Name of the input h5parm solution set
  - id: outsoltab
    type: string?
    inputBinding:
      position: 0
      prefix: '--outsoltab'
    doc: Name of the output h5parm solution set
  - id: antenna
    type: string?
    inputBinding:
      position: 0
      prefix: '--antenna'
    doc: Regular expression of antenna solutions to be transferred
  - id: do_transfer
    type: boolean?
    inputBinding:
      position: 0
      prefix: '--do_transfer True'
    doc: Enable/disable the task
  - id: trusted
    type: string?
    inputBinding:
      position: 0
      prefix: '--trusted'
    doc: Comma-separated list of sources to be trusted
  - id: max_separation_arcmin
    type: float?
    inputBinding:
      position: 0
      prefix: '--max_separation_arcmin'
    doc: define the maximum seperation between pointing and model direction in arcmin
  - id: parset
    type: File?
    inputBinding:
      position: 0
      prefix: '--parset'
    doc: Parset for plotting diagnostic plots after transfer with LoSoTo
outputs:
  - id: outh5parm
    type: File
    outputBinding:
      glob: $(inputs.h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: 'transfer_solutions*.log'
  - id: plots
    type: File[]
    outputBinding:
      glob: "*.png"

stdout: transfer_solutions.log
stderr: transfer_solutions_err.log
label: transfer_solutions
hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.h5parm)
        writable: true
