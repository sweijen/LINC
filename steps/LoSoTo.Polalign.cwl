
class: CommandLineTool
cwlVersion: v1.2
id: losoto_polalign

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
  
doc: |
    Estimate polarization misalignment as delay
   
requirements:
  InlineJavascriptRequirement:
    expressionLib:
     - |
        function get_config() {
        var par = ['soltab = ' + inputs.soltab]
        if (inputs.ncpu !== null) par.push('ncpu='+inputs.ncpu);
        par.push('[polalign]')
        par.push('operation = POLALIGN')
        for(var field_name in inputs){
            if(field_name === 'input_h5parm' ||
               field_name === 'soltab' ||
               field_name === 'ncpu') continue;
            if(inputs[field_name] === null ||
               inputs[field_name] === 'null') continue;
            par.push(field_name+'='+inputs[field_name])
        }
        return par
        }
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_config().join('\n'))
      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true



baseCommand: "losoto"

arguments:
  - '--verbose'
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: soltabout
    default: phasediff
    type: string?
    doc: output table name (same solset)
  - id: maxResidual
    type: float?
    default: 1
    doc: Maximum acceptable rms of the residuals in radians before flagging. Set to zero to avoid the check.
  - id: fitOffset
    type: boolean?
    default: false
    doc: Assume that together with a delay each station has also a differential phase offset (important for old LBA observations).
    
  - id: average
    type: boolean?
    default: false
    doc: Mean-average in time the resulting delays/offset.
  - id: replace
    type: boolean?
    default: false
    doc: replace using smoothed value instead of flag bad data? Smooth must be active.
  - id: refAnt
    type: string?
    doc: Reference antenna, if not set use the first one.
    
outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log
