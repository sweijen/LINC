class: CommandLineTool
cwlVersion: v1.2
id: check_unflagged_fraction
baseCommand:
  - python3
  - check_fraction.py
inputs:
    - id: msin
      type: Directory
      doc: MS to check
      inputBinding:
        position: 0
    - id: min_fraction
      type: float?
      default: 0.5
      doc: Fraction of flagged data in a MS to be acceptable
requirements:
  - class: InlineJavascriptRequirement
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: false
      - entryname: check_fraction.py
        entry: |
          import sys
          import json
          import os
          import casacore.tables as pt

          from check_unflagged_fraction import main as check_unflagged_fraction

          ms           = sys.argv[1]
          inputs       = json.loads(r"""$(inputs)""")
          msin         = inputs['msin']
          min_fraction = inputs['min_fraction']

          output = check_unflagged_fraction(ms, min_fraction = min_fraction, print_fraction = True)

          filename           = os.path.basename(output['filename'].rstrip('/'))
          unflagged_fraction = output['unflagged_fraction']
          flagged            = output['flagged']

          t = pt.table(ms, readonly = True, ack = False)
          tfreq = pt.table(t.getkeyword('SPECTRAL_WINDOW'), readonly = True, ack = False)
          ref_freq = tfreq.getcol('REF_FREQUENCY', nrow = 1)[0]
          
          cwl_output = {}
          cwl_output['unflagged_fraction']  = unflagged_fraction
          cwl_output['frequency']           = ref_freq

          if 'listing' in inputs['msin'].keys():
             del inputs['msin']['listing']
          if flagged != 'None':
             cwl_output['msout']            = [inputs['msin']]
             cwl_output['msout'][0]['path'] = output['filename']
             cwl_output['filename']         = 'None'
          else:
             cwl_output['msout']            = []
             cwl_output['filename']         = filename

          with open('./out.json', 'w') as fp:
              json.dump(cwl_output, fp)

outputs:
  - id: msout
    type: Directory[]
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).msout)
  - id: frequency
    type: float
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).frequency)
  - id: unflagged_fraction
    type: float
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).unflagged_fraction)
  - id: filenames
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).filename)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'check_unflagged_fraction*.log'

hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
  - class: ResourceRequirement
    coresMin: 8
stdout: check_unflagged_fraction.log
stderr: check_unflagged_fraction_err.log
