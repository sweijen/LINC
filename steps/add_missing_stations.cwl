class: CommandLineTool
cwlVersion: v1.2
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
id: add_missing_stations
baseCommand:
  - add_missing_stations.py
inputs:
  - default: output.h5
    id: h5parm
    type: File
    inputBinding:
      position: 1
    doc: H5parm to which this action should be performed.
  - id: refh5parm
    type: File
    inputBinding:
      position: 0
      prefix: '--refh5'
    doc: External H5parm from which the full list of antennas is used from.
  - id: solset
    type: string?
    inputBinding:
      position: 0
      prefix: '--solset'
    default: 'sol000'
    doc: Input calibration solutions
  - id: refsolset
    type: string?
    inputBinding:
      position: 0
      prefix: '--refsolset'
    doc: Input calibration solutions of the reference h5parm file
    default: 'sol000'
  - id: soltab_in
    type: string?
    inputBinding:
      position: 0
      prefix: '--soltab_in'
    doc: Input solution table
    default: 'phase000'
  - id: soltab_out
    type: string?
    inputBinding:
      position: 0
      prefix: '--soltab_out'
    doc: Output solution table (has to be different from input solution table)
    default: 'GSMphase'
  - id: filter
    type: string?
    inputBinding:
      position: 0
      prefix: '--filter'
    doc: Filter these antenna string from the processing
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    inputBinding:
      position: 0
      prefix: '--bad_antennas'
    doc: Antenna string to be processed
    default: '[CR]S*&'
outputs:
  - id: outh5parm
    type: File
    outputBinding:
      glob: $(inputs.h5parm.basename)
    format: 'lofar:#H5Parm'
  - id: log
    type: File[]
    outputBinding:
      glob: 'add_missing_stations*.log'

stdout: add_missing_stations.log
stderr: add_missing_stations_err.log
label: add_missing_stations
hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.h5parm)
        writable: true
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
