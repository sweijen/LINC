#! /bin/bash
set -e

DOCKER_TAG=latest

docker build ${PWD}/.. -f Dockerfile-base -t ${DOCKER_TAG} && \
docker build ${PWD}/.. -f Dockerfile-linc -t astronrd/linc:${DOCKER_TAG}