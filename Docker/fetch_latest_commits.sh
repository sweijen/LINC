#!/bin/sh

git ls-remote https://github.com/lofar-astron/LofarStMan HEAD       | awk '{ print "LOFARSTMAN_COMMIT="$1 }'
git ls-remote https://github.com/aroffringa/dysco.git HEAD          | awk '{ print "DYSCO_COMMIT="$1 }'
git ls-remote https://git.astron.nl/RD/idg.git HEAD                 | awk '{ print "IDG_COMMIT="$1 }'
git ls-remote https://gitlab.com/aroffringa/aoflagger.git HEAD      | awk '{ print "AOFLAGGER_COMMIT="$1 }'
git ls-remote https://github.com/nlesc-dirac/sagecal HEAD           | awk '{ print "SAGECAL_COMMIT="$1 }'
git ls-remote https://github.com/lofar-astron/LOFARBeam.git HEAD    | awk '{ print "LOFARBEAM_COMMIT="$1 }'
git ls-remote https://git.astron.nl/RD/EveryBeam.git HEAD           | awk '{ print "EVERYBEAM_COMMIT="$1 }'
git ls-remote https://git.astron.nl/RD/DP3.git HEAD                 | awk '{ print "DP3_COMMIT="$1 }'
git ls-remote https://gitlab.com/aroffringa/wsclean.git HEAD        | awk '{ print "WSCLEAN_COMMIT="$1 }'
git ls-remote https://git.astron.nl/RD/LSMTool.git HEAD             | awk '{ print "LSMTOOL_COMMIT="$1 }'
