# LINC
## The LOFAR Initial Calibration pipeline

**LINC** is a pipeline to correct for various instrumental and ionospheric effects in both **LOFAR HBA** and **LOFAR LBA** observations.
It will prepare your data so that you will be able to use any direction-dependent calibration software, like [Rapthor](https://github.com/darafferty/rapthor), [factor](https://github.com/lofar-astron/factor), or [killMS](https://github.com/saopicc/killMS/).

It includes:
* removal of clock offsets between core and remote stations (using clock-TEC separation)
* correction of the polarization alignment between XX and YY
* robust time-independent bandpass correction
* ionospheric RM corrections with [RMextract](https://github.com/lofar-astron/RMextract/)
* removal of the element beam
* advanced flagging and removal of bad data
* mitigation of broad-band RFI and bad stations
* direction-independent phase correction of the target, using a global sky model from [TGSS ADR](https://http://tgssadr.strw.leidenuniv.nl/)  or the new Global Sky Model [GSM](http://172.104.228.177/)
* detailled diagnostics

The full documentation can be found at the [LINC webpage](https://linc.readthedocs.io/).

**WARNING**: The current skymodel used for 3C295 is not using the Scaife&Heald flux density scale.

### Software requirements
* [DP3](https://github.com/lofar-astron/DP3) (v5.2 or later)
* [LoSoTo](https://github.com/revoltek/losoto) (v2.2.1 or later)
* [LSMTool](https://git.astron.nl/RD/LSMTool) (v1.4.8 or later)
* [EveryBeam](https://git.astron.nl/RD/EveryBeam) (v0.3.1 or later)
* [Sagecal](https://github.com/nlesc-dirac/sagecal) (v0.7.8 or later)
* [RMextract](https://github.com/maaijke/RMextract) (v0.4.4 or later)
* [AOFlagger](https://gitlab.com/aroffringa/aoflagger) (v3.1.0 or later)
* [WSClean](https://gitlab.com/aroffringa/wsclean) (v3.1 or later)
* [IDG](https://gitlab.com/astron-idg/idg) (v1.0.0 or later)
* [LofarStMan](https://github.com/lofar-astron/LofarStMan)
* [Dysco](https://github.com/aroffringa/dysco.git) (v1.2 or later)
* casacore
* Python3 (including matplotlib, scipy, and astropy)
* [cwltool](https://github.com/common-workflow-language/cwltool) (3.1.20220406080846 or later) [toil-cwl-runner](https://github.com/DataBiosphere/toil) (5.7.0a1 or later)

### Installation
Detailled installation instructions for LINC are available at the [LINC documentation](https://linc.readthedocs.io/en/latest/installation.html).

It is recommended to use [Docker](https://docs.docker.com/engine/install/ubuntu/) or [Singularity](https://sylabs.io/guides/3.0/user-guide/installation.html) (version 3.1 or later) for the use of software containers. **NOTE:** Due to a bug Debian/Ubuntu packages of Singularity version 3.9.9 will not run with **LINC**.

To download and install the repository run the following commands:

    git clone https://git.astron.nl/RD/LINC.git linc --depth 1
    cd linc
    pip3 install --upgrade $PWD

### Running LINC
Instructions how to [setup](https://linc.readthedocs.io/en/latest/parset.html) and [run](https://linc.readthedocs.io/en/latest/running.html) LINC are also available at the LINC documentation.

### Directory structure
LINC contains the following sub-directories:
* **Docker**: contains the Dockerfile for creating the Docker image which contains all necessary software packages
* **docs**: contains the documentation for LINC
* **rfistrategies**: strategies for statistical RFI mitigation using [AOFlagger](https://sourceforge.net/p/aoflagger/wiki/Home/)
* **scripts**: scripts that the pipeline calls to process data, generate plots, etc.
* **skymodels**: skymodels that are used by the pipeline (e.g. for demixing or calibrating the calibrator)
* **solutions**: template solutions for the use of non-supported calibrator sources
* **steps**: steps of the pipeline described in the [Common Workflow Language (CWL)](https://www.commonwl.org/)
* **test_jobs**: descriptions for running continouos integration (CI)
* **workflows**: pipeline workflow desriptions in the [Common Workflow Language (CWL)](https://www.commonwl.org/)

**LINC** and its scripts were developed by:
* Alexander Drabent
* David Rafferty
* Mattia Mancini
* Marcel Loose
* Andreas Horneffer
* Francesco de Gasperin
* Marco Iacobelli
* Emanuela Orru
* Björn Adebahr
* Martin Hardcastle
* George Heald
* Soumyajit Mandal
* Carole Roskowinski
* Jose Sabater Montes
* Timothy Shimwell
* Sarrvesh Sridhar
* Reinout van Weeren
* Wendy Williams

### Acknowledgements
<img src="https://www.egi.eu/wp-content/uploads/2020/01/eu-logo.jpeg" alt="EU Flag" width="80">
<img src="https://www.egi.eu/wp-content/uploads/2020/01/eosc-hub-v-web.png" alt="EOSC-hub logo" height="60">
This work is co-funded by the EOSC-hub project (Horizon 2020) under Grant number 777536.

This work was supported by the BMBF Verbundforschung under the grant 05A20STA.


The LINC procedure is described in this paper:
* de Gasperin, F.; Dijkema, T. J.; Drabent, A.; Mevius, M.; Rafferty, van Weeren, R., et al. 2019, [A&A, 662, A5](http://adsabs.harvard.edu/abs/2018arXiv181107954D)
