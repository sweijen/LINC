.. toctree::
   :hidden:
 
   pipelineoverview_old
   
.. _pipeline_overview:

Pipeline overview
=================
.. note::

   If you are running the deprecated genericpipeline version of the pipeline (**prefactor** 3.2 or older), please check the :doc:`old instructions page<pipelineoverview_old>`.

**LINC** is organized in three major parts to process **LOFAR** data:

    .. image:: LINC_CWL_workflow_sketch.png

``LINC_calibrator``
    Processes the (amplitude-)calibrator to derive direction-independent corrections. See :ref:`calibrator_pipeline` for details.
``LINC_target``
    Transfers the direction-independent corrections to the target and does direction-independent calibration of the target. See :ref:`target_pipeline` for details.
