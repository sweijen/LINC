.. LINC documentation master file, created by
   sphinx-quickstart on Tue Nov 27 11:30:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LOFAR Initial Calibration (**LINC**) Pipeline
=============================================

**LINC** is a pipeline to correct for various instrumental and ionospheric effects in both **LOFAR HBA** and **LOFAR LBA** observations.

It includes:

- removal of clock offsets between core and remote stations (using clock-TEC separation)
- correction of the polarization alignment between XX and YY
- robust time-independent bandpass correction
- ionospheric RM corrections with `RMextract`_
- removal of the element beam
- advanced flagging and interpolation of bad data
- mitigation of broad-band RFI and bad stations
- direction-independent phase correction of the target, using a global sky model from `TGSS ADR`_  or the new `Global Sky Model`_ (GSM)
- detailed diagnostics

It will prepare your data to allow you continuing the processing with any direction-dependent calibration software, like `Rapthor`_, `factor`_ or `killMS`_.

.. note::

   If you intend to use **LINC** for the processing of long baselines (international stations) the user is referred to the `LOFAR-VLBI documentation`_, since the compatibility of both pipelines may be limited to certain version or releases. It is recommended to stick to the instructions therein.
   
   **Note:** The current version of **LINC** does not yet support state-of-the-art calibration of target fields with the **LOFAR LBA**. An implementation is planned for upcoming releases. Please continue your processing using `LiLF`_.
    

Introduction
------------

.. toctree::
   :maxdepth: 2

   acknowledgements


Obtaining **LINC**
------------------

.. toctree::
   :maxdepth: 2

   installation
   changelog


Setting Up and Running **LINC**
-------------------------------

.. toctree::
   :maxdepth: 2

   preparation
   parset
   running
   help


The **LINC** Pipelines
----------------------

.. toctree::
   :maxdepth: 2

   pipelineoverview
   calibrator
   target


.. _LOFAR-VLBI documentation: https://lofar-vlbi.readthedocs.io/
.. _Rapthor: https://github.com/darafferty/rapthor
.. _Global Sky Model: https://lcs165.lofar.eu/
.. _TGSS ADR: https://tgssadr.strw.leidenuniv.nl/
.. _RMextract: https://github.com/lofar-astron/RMextract/
.. _factor: https://github.com/lofar-astron/factor/
.. _killMS: https://github.com/saopicc/killMS/
.. _LiLF: https://github.com/revoltek/LiLF
