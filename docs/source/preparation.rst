.. _data_preparation:

Preparing the data
------------------

**LINC** requires **LOFAR LBA** or **HBA** raw or pre-processed data. These data are
typically obtained from the LOFAR `Long-Term Archive`_.

- The calibrator and target data have to match, i.e., be observed close enough
  in time that calibration values can be transferred.

- For each observation you should process all the calibrator data at once
  together. Clock/TEC separation and flagging of bad amplitudes work better with
  the full bandwidth.

- For the target pipeline you will need to have internet access from the machine you are running **LINC**.
  It is required in order to retrieve RM values from `CODE`_ and a global sky model (`TGSS`_ or `GSM`_). Both are hosted as online services.
  It is also possible to provide an own target skymodel to **LINC** (using the parameters ``target_skymodel``, and ``use_target``, see the :doc:`target<target>` pipeline parameter information).

.. note::

    Processing of interleaved datasets is not currently supported.
    
    **LINC** can not handle multi-epoch observations at once.
    
    Older versions of **LINC**: All input measurement-sets for one pipeline run need to be in the same directory.

.. _CODE: http://ftp.aiub.unibe.ch/CODE/
.. _TGSS: http://tgssadr.strw.leidenuniv.nl/doku.php
.. _GSM:  http://172.104.228.177/
.. _Long-Term Archive: https://lta.lofar.eu
