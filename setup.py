import os, glob
from setuptools import setup


# Functions read() was copied from Pip package.
def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    with open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


data_files = []
for top in ('rfistrategies', 'skymodels', 'solutions', 'steps', 'workflows'):
    for root, _, files in os.walk(top):
        data_files.append(
            (os.path.join('share', 'linc', root), [os.path.join(root, f) for f in files])
        )

setup(
    name='LINC',
    version='4.0',
    description='LINC: The LOFAR Initial Calibration pipeline.',
    long_description=read("README.md"),
    long_description_content_type='text/markdown',
    url='https://git.astron.nl/RD/LINC',
    license='GNU GPL 3',
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'Intended Audience :: Science/Research',
        'Operating System :: POSIX :: Linux',
        'Topic :: Scientific/Engineering :: Astronomy'],
    platforms='any',
    install_requires=[
        'aplpy', 'astropy', 'bdsf', 'h5py', 'losoto==2.2.1', 'lsmtool',
        'matplotlib', 'numpy<1.23.0', 'python-casacore', 'RMextract', 'scipy'],
    scripts=glob.glob('scripts/*'),
    data_files=data_files,
    # explicitly declare packages so setuptools does not attempt auto discovery
    packages=[]
)
