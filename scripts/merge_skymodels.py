#!/usr/bin/env python
"""
Append a LOFAR skymodel to an existing one
"""

import os, logging
import lsmtool

########################################################################

def main(inmodel1, inmodel2, outmodel = 'output.skymodel'):
    
    logging.info('Reading ' + inmodel1)
    s1 = lsmtool.load(inmodel1)
    logging.info('Reading ' + inmodel2)
    s2 = lsmtool.load(inmodel2)

    logging.info('Adding skymodel ' + inmodel2 + ' to ' + inmodel1)
    s1.concatenate(s2)
    s1.write(outmodel)

    return(0)

########################################################################
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Merge two LOFAR skymodel text files.')

    parser.add_argument('--inmodel1', type=str, default=None, help='input/output skymodel.')
    parser.add_argument('--inmodel2', type=str, default=None, help='skymodel to append')
    parser.add_argument('--outmodel', type=str, default='output.skymodel', help='output skymodel name')

    args = parser.parse_args()

    format_stream = logging.Formatter("%(asctime)s\033[1m %(levelname)s:\033[0m %(message)s","%Y-%m-%d %H:%M:%S")
    format_file   = logging.Formatter("%(asctime)s %(levelname)s: %(message)s","%Y-%m-%d %H:%M:%S")
    logging.root.setLevel(logging.INFO)
 
    main(inmodel1 = os.path.expandvars(args.inmodel1), inmodel2 = os.path.expandvars(args.inmodel2), outmodel = os.path.expandvars(args.outmodel))
