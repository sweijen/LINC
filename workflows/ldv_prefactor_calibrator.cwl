#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement
- class: SubworkflowFeatureRequirement

inputs:
- id: surls
  type: string[]

outputs:
- id: solutions
  type: File
  outputSource:
  - prefactor_calibrator/solutions
- id: summary
  type: File
  outputSource:
  - prefactor_calibrator/summary
- id: inspection_plots
  type: File
  outputSource:
  - compress_inspection_plots/compressed
- id: log_files
  type: File
  outputSource:
  - compress_logs/compressed
steps:
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter: surl_link
  run: ../steps/fetch_data.cwl
  out:
  - id: uncompressed
- id: prefactor_calibrator
  in:
  - id: msin
    source: fetch_data/uncompressed
  run: HBA_calibrator.cwl
  out:
  - id: solutions
  - id: summary
  - id: inspection_plots
  - id: log_files
- id: compress_inspection_plots
  in: 
  - id: directory
    source: prefactor_calibrator/inspection_plots
  out:
  - id: compressed
  run: ../steps/compress.cwl
- id: compress_logs
  in:
  - id: directory
    source: prefactor_calibrator/log_files
  out:
  - id: compressed
  run: ../steps/compress.cwl