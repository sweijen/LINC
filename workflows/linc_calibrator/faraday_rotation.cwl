class: Workflow
cwlVersion: v1.2
id: faraday_rotation
label: faraday_rotation
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
inputs:
  - id: refAnt
    type: string?
    default: 'CS001HBA0'
  - id: input_h5parm
    type: File
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_faraday/output_h5parm
    type: File
    format: 'lofar:#H5Parm'
  - id: logfiles
    outputSource:
      - losoto_duplicate/log
      - losoto_faraday/log
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: losoto_duplicate
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: losoto_faraday
    in:
      - id: input_h5parm
        source: losoto_duplicate/output_h5parm
      - id: soltab
        default: sol000/rotation000
      - id: soltabout
        default: faraday
      - id: refAnt
        source: refAnt
      - id: maxResidual
        default: 1
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Faraday.cwl
requirements: []
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
