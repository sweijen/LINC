class: Workflow
cwlVersion: v1.2
id: losoto_ion
label: losoto_ion
inputs:
  - id: input_h5parm
    type: File
  - id: fit3rdorder
    type: boolean?
    default: false
  - id: refAnt
    type: string?
    default: 'CS001HBA0'
  - id: maxStddev
    type: float?
    default: -1.0
  - id: clock_smooth
    type: boolean?
    default: true
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_flagstation/output_h5parm
    type: File
  - id: logfiles
    outputSource:
      - duplicatePbkp/log
      - losoto_clocktec/log
      - losoto_residual/log
      - duplicateCbkp/log
      - smooth/logfile
      - losoto_flagstation/log
    type: File[]
    linkMerge: merge_flattened
  - id: parset
    outputSource:
      - losoto_residual/output_h5parm
    type: File
steps:
  - id: duplicatePbkp
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: losoto_clocktec
    in:
      - id: input_h5parm
        source: duplicatePbkp/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: clocksoltabOut
        default: clock
      - id: tecsoltabOut
        default: tec
      - id: offsetsoltabOut
        default: phase_offset
      - id: tec3rdsoltabOut
        default: tec3rd
      - id: FlagBadChannels
        default: false
      - id: CombinePol
        default: false
      - id: Fit3rdOrder
        source: fit3rdorder
      - id: Circular
        default: false
    out:
      - id: output_h5parm
      - id: parset
      - id: log
    run: ../../steps/LoSoTo.ClockTec.cwl
  - id: duplicateCbkp
    in:
      - id: input_h5parm
        source: losoto_clocktec/output_h5parm
      - id: soltab
        default: sol000/clock
      - id: soltabOut
        default: OrigClock
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: smooth
    in:
      - id: input_h5parm
        source: duplicateCbkp/output_h5parm
      - id: execute
        source: clock_smooth
      - id: soltab
        default: sol000/clock
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: replace
        default: true
      - id: log
        default: false
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Smooth.cwl
  - id: losoto_residual
    doc: |
      Note: this step uses the OrigClock solutions, to ensure that the
      unsmoothed clock is used to calculate the residuals
    in:
      - id: input_h5parm
        source: smooth/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabsToSub
        source:
          - fit3rdorder
        valueFrom: '$(self ? ["tec", "OrigClock", "tec3rd"] : ["tec", "OrigClock"])'
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Residual.cwl
  - id: losoto_flagstation
    in:
      - id: input_h5parm
        source: losoto_residual/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: mode
        default: resid
      - id: maxStddev
        source: maxStddev
      - id: refAnt
        source: refAnt
      - id: soltabExport
        default: clock
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.FlagStation.cwl
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
