class: Workflow
cwlVersion: v1.2
id: apply_calibrate
label: apply_calibrate
inputs:
  - id: msin
    type: Directory
  - id: do_smooth
    type: boolean?
    default: false
  - id: flagunconverged
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: input_h5parm
    type: File
outputs:
  - id: msout
    outputSource:
      - calib_cal/msout
    type: Directory
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
  - id: apply_cal.log
    outputSource:
      - concat_logfiles_applycal/output
    type: File
  - id: calib_cal.log
    outputSource:
      - concat_logfiles_calib_cal/output
    type: File
  - id: outh5parm
    outputSource:
      - calib_cal/h5parm
    type: File
  - id: applybeam.log
    outputSource:
      - concat_logfiles_applybeam/output
    type: File
steps:
  - id: applyPA
    in:
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        default: polalign
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: applyPA
  - id: applybeam
    in:
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: updateweights
        default: 'true'
      - id: invert
        default: 'true'
      - id: beammode
        default: element
      - id: usechannelfreq
        default: 'false'
      - id: msin
        source: applyPA/msout
      - id: type
        default: applybeam
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applybeam.cwl
    label: applybeam
  - id: BLsmooth
    in:
      - id: msin
        source: applybeam/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CORRECTED_DATA
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: calib_cal
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: flagunconverged
        source: flagunconverged
      - id: propagate_solutions
        source: propagatesolutions
      - id: mode
        default: rotation+diagonal
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
  - id: concat_logfiles_calib_cal
    in:
      - id: file_list
        source:
          - calib_cal/logfile
      - id: file_prefix
        default: calib_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_cal
  - id: concat_logfiles_applybeam
    in:
      - id: file_list
        source:
          - applybeam/logfile
      - id: file_prefix
        default: applybeam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applybeam
  - id: concat_logfiles_applycal
    in:
      - id: file_list
        source:
          - applyPA/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applycal
requirements: []
