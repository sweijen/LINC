class: Workflow
cwlVersion: v1.2
id: pa
label: PA
inputs:
  - id: flagunconverged
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: msin
    type: Directory[]
  - id: h5parm
    type: File
  - id: refant
    type: string?
    default: 'CS001HBA0'
  - id: inh5parm_logfile
    type: File[]
  - id: do_smooth
    type: boolean?
    default: false
  - id: fit_offset_PA
    type: boolean?
    default: false
outputs:
  - id: outh5parm
    outputSource:
      - h5parm_collector/outh5parm
    type: File
  - id: msout
    outputSource:
      - apply_calibrate_pa/msout
    type: Directory[]
  - id: inspection
    outputSource:
      - losoto_plot_P3/output_plots
      - losoto_plot_Pd/output_plots
      - losoto_plot_Rot3/output_plots
      - losoto_plot_A3/output_plots
      - losoto_plot_Align/output_plots
      - losoto_plot_Pr/output_plots
      - losoto_plot_Pr2/output_plots
    type: File[]
    linkMerge: merge_flattened
  - id: outsolutions
    outputSource:
      - write_solutions/outh5parm
    type: File
  - id: logfiles
    outputSource:
      - concat_logfiles_pa/output
      - concat_logfiles_calib/output
      - concat_logfiles_blsmooth/output
      - concat_logfiles_beam/output
      - concat_logfiles_apply/output
    type: File[]
    linkMerge: merge_flattened
  - id: outh5parm_logfile
    outputSource:
      - h5parm_collector/log
    type: File[]
steps:
  - id: pol_align
    in:
      - id: refAnt
        default: CS001HBA0
        source: refant
      - id: input_h5parm
        source: h5parm
      - id: fit_offset_PA
        source: fit_offset_PA
    out:
      - id: output_h5parm
      - id: logfiles
    run: ./pol_align.cwl
    label: PolAlign
  - id: losoto_plot_P3
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/phaseOrig
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: polalign_ph_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_P3
  - id: losoto_plot_Pd
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/phaseOrig
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: polalign_ph_poldif
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd
  - id: losoto_plot_Rot3
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/rotation000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: polalign_rotangle
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Rot3
  - id: losoto_plot_A3
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: polalign_amp_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_A3
  - id: losoto_plot_Align
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/polalign
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: polalign
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Align
  - id: losoto_plot_Pr
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: polalign_ph-res_poldif
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pr
  - id: losoto_plot_Pr2
    in:
      - id: input_h5parm
        source: pol_align/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: polalign_ph-res_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pr2
  - id: concat_logfiles_pa
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - pol_align/logfiles
          - losoto_plot_P3/logfile
          - losoto_plot_Pd/logfile
          - losoto_plot_Rot3/logfile
          - losoto_plot_A3/logfile
          - losoto_plot_Align/logfile
          - losoto_plot_Pr/logfile
          - losoto_plot_Pr2/logfile
          - write_solutions/log
      - id: file_prefix
        default: losoto_PA
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_PA
  - id: write_solutions
    in:
      - id: h5parmFiles
        source:
          - pol_align/output_h5parm
      - id: outsolset
        default: calibrator
      - id: insoltab
        default: polalign
      - id: outh5parmname
        default: cal_solutions.h5
      - id: squeeze
        default: true
      - id: verbose
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: write_solutions
  - id: apply_calibrate_pa
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: input_h5parm
        source: write_solutions/outh5parm
    out:
      - id: msout
      - id: BLsmooth.log
      - id: apply_cal.log
      - id: calib_cal.log
      - id: outh5parm
      - id: applybeam.log
    run: ./apply_calibrate_pa.cwl
    label: apply_calibrate_pa
    scatter:
      - msin
  - id: concat_logfiles_blsmooth
    in:
      - id: file_list
        source:
          - apply_calibrate_pa/BLsmooth.log
      - id: file_prefix
        default: blsmooth_PA
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_blsmooth
  - id: concat_logfiles_beam
    in:
      - id: file_list
        source:
          - apply_calibrate_pa/applybeam.log
      - id: file_prefix
        default: applybeam_PA
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_beam
  - id: concat_logfiles_apply
    in:
      - id: file_list
        source:
          - apply_calibrate_pa/apply_cal.log
      - id: file_prefix
        default: apply_cal_PA
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply
  - id: concat_logfiles_calib
    in:
      - id: file_list
        source:
          - apply_calibrate_pa/calib_cal.log
      - id: file_prefix
        default: calib_cal_PA
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - apply_calibrate_pa/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: H5parm_collector
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
