class: Workflow
cwlVersion: v1.2
id: prep
label: prep
inputs:
  - id: msin
    type: Directory[]
  - id: filter_baselines
    type: string?
    default: '*&'
  - id: raw_data
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: flagunconverged
    type: boolean?
    default: false
  - id: demix
    type: boolean?
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: memoryperc
    type: int?
    default: 20
  - id: flag_baselines
    type: string[]?
    default: []
  - id: avg_timeresolution
    type: int?
    default: 4
  - id: avg_freqresolution
    type: string?
    default: '48.82kHz'
  - id: process_baselines_cal
    type: string?
    default: '*&'
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: demix_sources
    type: string[]?
    default: []
  - id: min_separation
    type: int?
    default: 30
  - id: do_smooth
    type: boolean?
    default: false
  - id: max_separation_arcmin
    type: float?
    default: 1.0
  - id: calibrator_path_skymodel
    type:
      - Directory?
      - File?
  - id: A-Team_skymodel
    type: File?
  - id: elevation
    type: string?
    default: 0deg..15deg
  - id: amplmin
    type: float?
    default: 1.e-30
  - id: rfistrategy
    type:
      - File?
      - string?
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
outputs:
  - id: outh5parm
    outputSource:
      - h5parm_collector/outh5parm
    type: File
  - id: check_Ateam_separation.png
    outputSource:
      - check_ateam_separation/output_imag
    type: File
  - id: msout
    outputSource:
      - predict_calibrate/msout
    type: Directory[]
  - id: calibrator_name
    outputSource:
      - find_skymodel_cal/model_name
    type: string
  - id: outh5parm_logfile
    outputSource:
      - h5parm_collector/log
    type: File[]
  - id: check_Ateam_separation.json
    outputSource:
      - check_ateam_separation/output_json
    type: File
  - id: flagged_fraction_dict_refant
    outputSource:
      - predict_calibrate/flagged_fraction_dict
    type: string[]
  - id: flagged_fraction_dict
    outputSource:
      - dp3_prep_cal/flagged_fraction_dict
    type: string[]
  - id: logfiles
    outputSource:
      - concat_logfiles_calib/output
      - concat_logfiles_predict/output
      - concat_logfiles_blsmooth/output
      - concat_logfiles_skymodels/output
      - find_skymodel_cal/logfile
      - concat_logfiles_prep_cal/output
      - check_ateam_separation/logfile
      - check_demix/logfile
      - aoflag/logfile
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: select
    in:
      - id: input
        source:
          - msin
    out:
      - id: output
    run: ../../steps/selectfirstdirectory.cwl
    label: select_only_first
  - id: dp3_prep_cal
    in:
      - id: baselines_to_flag
        default: []
        source:
          - flag_baselines
      - id: elevation_to_flag
        source: elevation
      - id: min_amplitude_to_flag
        source: amplmin
      - id: memoryperc
        default: 20
        source: memoryperc
      - id: raw_data
        default: false
        source: raw_data
      - id: demix
        source: check_demix/out_demix
      - id: msin
        linkMerge: merge_flattened
        source:
          - msin
      - id: msin_baseline
        default: '*'
        source: filter_baselines
      - id: skymodel
        source: merge_skymodels/skymodel_out
      - id: timeresolution
        default: 1
        source: avg_timeresolution
      - id: freqresolution
        default: 12.21kHz
        source: avg_freqresolution
      - id: demix_timeres
        source: demix_timeres
      - id: demix_freqres
        source: demix_freqres
      - id: demix_baseline
        default: '*'
        source: process_baselines_cal
      - id: target_source
        source: find_skymodel_cal/model_name
      - id: ntimechunk
        default: 10
        source: max_dp3_threads
      - id: subtract_sources
        source: check_demix/out_demix_sources
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
    out:
      - id: msout
      - id: flagged_fraction_dict
      - id: logfile
    run: ./dp3_prep_cal.cwl
    label: dp3_prep_cal
    scatter:
      - msin
  - id: predict_calibrate
    in:
      - id: msin
        linkMerge: merge_flattened
        source:
          - aoflag/output_ms
      - id: do_smooth
        source: do_smooth
      - id: sourcedb
        source: find_skymodel_cal/output_models
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
    out:
      - id: msout
      - id: BLsmooth.log
      - id: predict_cal.log
      - id: calib_cal.log
      - id: flagged_fraction_dict
      - id: outh5parm
    run: ./predict_calibrate.cwl
    label: predict_calibrate
    scatter:
      - msin
  - id: aoflag
    in:
      - id: msin
        linkMerge: merge_flattened
        source: dp3_prep_cal/msout
      - id: verbose
        default: true
      - id: concatenate-frequency
        default: true
      - id: strategy
        source: rfistrategy
      - id: reorder
        source: aoflag_reorder
      - id: chunk-size
        source: aoflag_chunksize
    out:
      - id: output_ms
      - id: logfile
    run: ../../steps/aoflag.cwl
    label: aoflag
  - id: check_ateam_separation
    in:
      - id: ms
        source:
          - select/output
      - id: min_separation
        source: min_separation
    out:
      - id: output_imag
      - id: output_json
      - id: logfile
    run: ../../steps/check_ateam_separation.cwl
    label: check_Ateam_separation
  - id: check_demix
    in:
      - id: demix
        source: demix
      - id: skymodel
        source: A-Team_skymodel
      - id: demix_sources
        source: demix_sources
      - id: clip_sources
        default: []
      - id: Ateam_separation_file
        source: check_ateam_separation/output_json
    out:
      - id: out_demix
      - id: out_demix_sources
      - id: logfile
    run: ../../steps/check_demix.cwl
    label: check_demix
  - id: find_skymodel_cal
    in:
      - id: msin
        source:
          - select/output
      - id: skymodels
        source: calibrator_path_skymodel
      - id: max_separation_arcmin
        source: max_separation_arcmin
    out:
      - id: output_models
      - id: model_name
      - id: logfile
    run: ../../steps/find_skymodel_cal.cwl
    label: find_skymodel_cal
  - id: merge_skymodels
    in:
      - id: inmodel1
        source: find_skymodel_cal/output_models
      - id: inmodel2
        source: A-Team_skymodel
    out:
      - id: skymodel_out
      - id: logfile
    run: ../../steps/merge_skymodels.cwl
    label: merge_skymodels
  - id: concat_logfiles_prep_cal
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - dp3_prep_cal/logfile
      - id: file_prefix
        default: dp3_prep_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_prep_cal
  - id: concat_logfiles_skymodels
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - merge_skymodels/logfile
      - id: file_prefix
        default: merge_skymodels
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_skymodels
  - id: concat_logfiles_blsmooth
    in:
      - id: file_list
        source:
          - predict_calibrate/BLsmooth.log
      - id: file_prefix
        default: blsmooth_prep
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_blsmooth
  - id: concat_logfiles_predict
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - predict_calibrate/predict_cal.log
      - id: file_prefix
        default: predict_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_predict
  - id: concat_logfiles_calib
    in:
      - id: file_list
        source:
          - predict_calibrate/calib_cal.log
      - id: file_prefix
        default: calib_cal_prep
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - predict_calibrate/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: H5parm_collector

requirements:
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
