class: Workflow
cwlVersion: v1.2
id: ion
label: ion
inputs:
  - id: filter_baselines
    type: string?
    default: '*&'
  - id: demix_sources
    type: string[]?
    default:
      - CasA
      - CygA
  - id: demix
    type: boolean?
    default: false
  - id: flags
    type: File[]
  - id: calibrator_name
    type: string?
    default: 'POINTING'
  - id: clock_smooth
    type: boolean?
    default: true
  - id: ion_3rd
    type: boolean?
    default: false
  - id: refant
    type: string?
    default: 'CS001HBA0'
  - id: h5parm
    type: File
  - id: tables2export
    type: string?
    default: 'clock'
  - id: inh5parm_logfile
    type: File[]
  - id: maxStddev
    type: float?
    default: -1.0
  - id: insolutions
    type: File
  - id: check_Ateam_separation.json
    type: File
outputs:
  - id: summary_file
    outputSource:
      - summary/summary_file
    type: File
  - id: inspection
    outputSource:
      - losoto_plot_P3/output_plots
      - losoto_plot_tec/output_plots
      - losoto_plot_tec3rd/output_plots
      - losoto_plot_clock/output_plots
      - losoto_plot_Pr/output_plots
    type: File[]
    linkMerge: merge_flattened
  - id: outsolutions
    outputSource:
      - h5parm_pointingname/outh5parm
    type: File
  - id: logfiles
    outputSource:
      - concat_logfiles_ion/output
      - concat_logfiles_summary/output
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: clocktec
    in:
      - id: input_h5parm
        source: h5parm
      - id: maxStddev
        source: maxStddev
      - id: fit3rdorder
        source: ion_3rd
      - id: clock_smooth
        source: clock_smooth
    out:
      - id: output_h5parm
      - id: logfiles
      - id: parset
    run: ./clocktec.cwl
    label: clocktec
  - id: losoto_plot_P3
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/phaseOrig
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ion_ph
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_P3
  - id: losoto_plot_tec
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/tec
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: false
      - id: refAnt
        source: refant
      - id: prefix
        default: tec
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_tec
  - id: losoto_plot_tec3rd
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: 'sol000/tec3rd'
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: false
      - id: refAnt
        source: refant
      - id: prefix
        default: tec3rd
      - id: execute
        source: ion_3rd
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_tec3rd
  - id: losoto_plot_clock
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/OrigClock
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: false
      - id: refAnt
        source: refant
      - id: prefix
        default: clock
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_clock
  - id: losoto_plot_Pr
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ion_ph-res
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pr
  - id: summary
    in:
      - id: flagFiles
        source: flags
        linkMerge: merge_flattened
      - id: pipeline
        default: 'LINC'
      - id: run_type
        default: 'calibrator'
      - id: filter
        source: filter_baselines
      - id: bad_antennas
        default: ''
      - id: Ateam_separation_file
        source: check_Ateam_separation.json
      - id: solutions
        source: h5parm_pointingname/outh5parm
      - id: demix
        source: demix
        valueFrom: '$(self ? "True" : "False")'
      - id: demix_sources
        source: demix_sources
        valueFrom: "$(self.join(','))"
    out:
      - id: summary_file
      - id: logfile
    run: ../../steps/summary.cwl
    label: summary
  - id: concat_logfiles_ion
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - clocktec/logfiles
          - losoto_plot_P3/logfile
          - losoto_plot_tec/logfile
          - losoto_plot_tec3rd/logfile
          - losoto_plot_clock/logfile
          - losoto_plot_Pr/logfile
          - write_solutions/log
          - h5parm_pointingname/log
      - id: file_prefix
        default: losoto_ion
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_ion
  - id: concat_logfiles_summary
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - summary/logfile
      - id: file_prefix
        source: calibrator_name
        valueFrom: $(self+'_summary')
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_summary
  - id: write_solutions
    in:
      - id: h5parmFile
        source: clocktec/output_h5parm
      - id: outsolset
        default: calibrator
      - id: insoltab
        source: tables2export
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
  - id: h5parm_pointingname
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solsetName
        default: calibrator
      - id: pointing
        source: calibrator_name
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parm_pointingname.cwl
    label: h5parm_pointingname
requirements:
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
  - class: SubworkflowFeatureRequirement
  - class: MultipleInputFeatureRequirement
