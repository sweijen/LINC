class: Workflow
cwlVersion: v1.2
id: pol_align
label: PolAlign
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
inputs:
  - id: refAnt
    type: string?
    default: 'CS001HBA0'
  - id: input_h5parm
    type: File
  - id: fit_offset_PA
    type: boolean?
    default: false
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_residual/output_h5parm
    type: File
    format: lofar:#H5Parm
  - id: logfiles
    outputSource:
      - losoto_duplicate/log
      - losoto_polalign/log
      - losoto_residual/log
    linkMerge: merge_flattened
    type: File[]

steps:
  - id: losoto_duplicate
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: losoto_polalign
    in:
      - id: input_h5parm
        source: losoto_duplicate/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabout
        default: polalign
      - id: average
        default: true
      - id: replace
        default: true
      - id: maxResidual
        default: 1.0
      - id: fitOffset
        source: fit_offset_PA
      - id: refAnt
        source: refAnt
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Polalign.cwl
  - id: losoto_residual
    in:
      - id: input_h5parm
        source: losoto_polalign/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabsToSub
        default:
          - polalign
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Residual.cwl
requirements:
  MultipleInputFeatureRequirement: {}
