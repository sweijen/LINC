class: Workflow
cwlVersion: v1.2
id: predict_calibrate
label: predict_calibrate
inputs:
  - id: msin
    type: Directory
  - id: do_smooth
    type: boolean?
    default: false
  - id: sourcedb
    type:
      - File
      - Directory
  - id: flagunconverged
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
outputs:
  - id: msout
    outputSource:
      - calib_cal/msout
    type: Directory
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
  - id: predict_cal.log
    outputSource:
      - concat_logfiles_predict/output
    type: File
  - id: calib_cal.log
    outputSource:
      - concat_logfiles_calib_cal/output
    type: File
  - id: flagged_fraction_dict
    outputSource:
      - calib_cal/flagged_fraction_dict
    type: string
  - id: outh5parm
    outputSource:
      - calib_cal/h5parm
    type: File
steps:
  - id: BLsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: predict
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: sources_db
        source: sourcedb
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: beammode
        default: 'array_factor'
    out:
      - id: msout
      - id: logfile
    run: ../../steps/predict.cwl
  - id: calib_cal
    in:
      - id: msin
        source: predict/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: propagate_solutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: mode
        default: rotation+diagonal
    out:
      - id: msout
      - id: h5parm
      - id: flagged_fraction_dict
      - id: logfile
    run: ../../steps/ddecal.cwl
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - predict/logfile
      - id: file_prefix
        default: predict
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_predict
  - id: concat_logfiles_calib_cal
    in:
      - id: file_list
        source:
          - calib_cal/logfile
      - id: file_prefix
        default: calib_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_cal
requirements: []
