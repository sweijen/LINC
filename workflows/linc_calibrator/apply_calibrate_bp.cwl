class: Workflow
cwlVersion: v1.2
id: apply_calibrate
label: apply_calibrate
inputs:
  - id: msin
    type: Directory
  - id: do_smooth
    type: boolean?
    default: false
  - id: flagunconverged
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: input_h5parm
    type: File
outputs:
  - id: apply_fr.log
    outputSource:
      - concat_logfiles_applyFR/output
    type: File
  - id: final_flags_out
    outputSource:
      - calib_cal/flagged_fraction_dict
    type: string?
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
  - id: apply_cal.log
    outputSource:
      - concat_logfiles_applyBP/output
    type: File
  - id: calib_cal.log
    outputSource:
      - concat_logfiles_calib_cal/output
    type: File
  - id: outh5parm
    outputSource:
      - calib_cal/h5parm
    type: File
  - id: apply_pa.log
    outputSource:
      - concat_logfiles_applyPA/output
    type: File
  - id: applybeam.log
    outputSource:
      - concat_logfiles_applybeam/output
    type: File
steps:
  - id: applyPA
    in:
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: correction
        default: polalign
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: applyPA
  - id: applyBP
    in:
      - id: msin
        source: applyPA/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: correction
        default: bandpass
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: updateweights
        default: 'True'
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: applyBP
  - id: applybeam
    in:
      - id: msin
        source: applyBP/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: type
        default: applybeam
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: updateweights
        default: 'true'
      - id: usechannelfreq
        default: 'false'
      - id: invert
        default: 'true'
      - id: beammode
        default: element
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applybeam.cwl
    label: applybeam
  - id: applyFR
    in:
      - id: msin
        source: applybeam/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: correction
        default: faraday
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: applyFR
  - id: BLsmooth
    in:
      - id: msin
        source: applyFR/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CORRECTED_DATA
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: calib_cal
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: propagate_solutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: mode
        default: scalarphase
    out:
      - id: msout
      - id: h5parm
      - id: flagged_fraction_dict
      - id: logfile
    run: ../../steps/ddecal.cwl
  - id: concat_logfiles_calib_cal
    in:
      - id: file_list
        source:
          - calib_cal/logfile
      - id: file_prefix
        default: calib_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_cal
  - id: concat_logfiles_applyFR
    in:
      - id: file_list
        source:
          - applyFR/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applyFR
  - id: concat_logfiles_applybeam
    in:
      - id: file_list
        source:
          - applybeam/logfile
      - id: file_prefix
        default: applybeam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applybeam
  - id: concat_logfiles_applyBP
    in:
      - id: file_list
        source:
          - applyBP/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applyBP
  - id: concat_logfiles_applyPA
    in:
      - id: file_list
        source:
          - applyPA/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applyPA
requirements: []
