class: Workflow
cwlVersion: v1.2
id: losoto_bandpass
label: losoto_bandpass
inputs:
  - id: ampRange
    type: float[]?
    default: [0, 0]
  - id: skipInternational
    type: boolean?
    default: false
  - id: input_h5parm
    type: File
  - id: max2interpolate
    type: int?
    default: 30
  - id: bandpass_freqresolution
    type: string?
    default: '195.3125kHz'
  - id: avg_freqresolution
    type: string?
    default: '48.82kHz'
outputs:
  - id: output_h5parm
    outputSource:
      - smoothb/output_h5parm
    type: File
  - id: logfiles
    outputSource:
      - duplicateAbkp/log
      - losoto_flag/log
      - flagbp/log
      - flagextend/log
      - merge/log
      - duplicateAbkp2/log
      - smooth/logfile
      - bandpass/logfile
      - interp/logfile
      - smoothb/logfile
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: duplicateAbkp
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: soltabOut
        default: amplitudeOrig000
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: losoto_flag
    in:
      - id: input_h5parm
        source: duplicateAbkp/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToFlag
        default:
          - time
          - freq
      - id: order
        default:
          - 100
          - 40
      - id: maxCycles
        default: 1
      - id: maxRms
        default: 5
      - id: mode
        default: smooth
      - id: preflagzeros
        default: false
      - id: replace
        default: false
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Flag.cwl
    label: flag
  - id: flagbp
    in:
      - id: input_h5parm
        source: losoto_flag/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: mode
        default: bandpass
      - id: ampRange
        source:
          - ampRange
      - id: skipInternational
        source: skipInternational
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.FlagStation.cwl
  - id: flagextend
    in:
      - id: input_h5parm
        source: flagbp/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToExt
        default:
          - time
          - freq
      - id: size
        default:
          - 200
          - 80
      - id: percent
        default: 50
      - id: maxCycles
        default: 2
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Flagextend.cwl
  - id: merge
    in:
      - id: input_h5parm
        source: flagextend/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: mode
        default: copy
      - id: soltabImport
        default: amplitude000
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Reweight.cwl
  - id: duplicateAbkp2
    in:
      - id: input_h5parm
        source: merge/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: soltabOut
        default: amplitudeOrig001
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: smooth
    in:
      - id: input_h5parm
        source: duplicateAbkp2/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: log
        default: true
      - id: replace
        default: false
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Smooth.cwl
  - id: bandpass
    in:
      - id: input_h5parm
        source: smooth/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToSmooth
        default:
          - freq
      - id: size
        default:
          - 17
      - id: mode
        default: savitzky-golay
      - id: degree
        default: 2
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Smooth.cwl
  - id: interp
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axisToRegrid
        default: freq
      - id: outSoltab
        default: bandpass
      - id: newdelta
        source: bandpass_freqresolution
      - id: delta
        source: avg_freqresolution
      - id: maxFlaggedWidth
        source: max2interpolate
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Interpolate.cwl
  - id: smoothb
    in:
      - id: input_h5parm
        source: interp/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: replace
        default: true
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Smooth.cwl
    label: smoothb
requirements: []
