class: Workflow
cwlVersion: v1.2
id: gsmcal
label: gsmcal
inputs:
  - id: msin
    type: Directory[]
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
  - id: num_SBs_per_group
    type: int?
    default: 10
  - id: reference_stationSB
    type: int?
    default: null
  - id: target_skymodel
    type: 
      - File
      - Directory
  - id: do_smooth
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: avg_timeresolution_concat
    type: int?
    default: 8
  - id: avg_freqresolution_concat
    type: string?
    default: '97.64kHz'
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
  - id: refant
    type: string?
    default: 'CS001HBA0'
  - id: rfistrategy
    type:
      - File?
      - string?
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
outputs:
  - id: msout
    outputSource:
      - calibrate_target/msout
    type: Directory[]
  - id: outh5parm
    outputSource:
      - h5parm_collector/outh5parm
    type: File
  - id: bad_antennas
    outputSource:
      - identifybadantennas_join/filter_out
    type: string
  - id: outh5parm_logfile
    outputSource:
      - concat_logfiles_losoto/output
    type: File
  - id: Ateam_flags_join_out
    outputSource:
      - Ateam_flags_join/flagged_fraction_antenna
    type: File
  - id: inspection
    outputSource:
      - losoto_plot_P/output_plots
      - losoto_plot_P2/output_plots
      - losoto_plot_Pd/output_plots
      - losoto_plot_Pd2/output_plots
      - plot_unflagged/output_imag
    type: File[]
    linkMerge: merge_flattened
  - id: out_refant
    outputSource:
      - findRefAnt_join/refant
    type: string
  - id: logfiles
    outputSource:
      - concat_logfiles_identify/output
      - concat_logfiles_RefAnt/output
      - sort_times_into_freqGroups/logfile
      - concat_logfiles_calib/output
      - concat_logfiles_dp3concat/output
      - concat_logfiles_blsmooth/output
      - concat_logfiles_unflagged/output
      - aoflag/logfile
    type: File[]
    linkMerge: merge_flattened
  - id: removed_bands
    outputSource:
      - check_unflagged_fraction/filenames
    type: string[]
  - id: total_bandwidth
    outputSource:
      - sort_times_into_freqGroups/total_bandwidth
    type: int
steps:
  - id: identifybadantennas
    in:
      - id: msin
        source: msin
    out:
      - id: flaggedants
      - id: logfile
    run: ../../steps/identify_bad_antennas.cwl
    label: identifybadantennas
    scatter:
      - msin
  - id: identifybadantennas_join
    in:
      - id: flaggedants
        source:
          - identifybadantennas/flaggedants
      - id: filter
        source: filter_baselines
    out:
      - id: filter_out
      - id: logfile
    run: ../../steps/identify_bad_antennas_join.cwl
    label: identifybadantennas_join
  - id: findRefAnt_join
    in:
      - id: flagged_fraction_dict
        source:
          - concat/flagged_fraction_dict
      - id: filter_station
        source: refant
    out:
      - id: refant
      - id: logfile
    run: ../../steps/findRefAnt_join.cwl
    label: findRefAnt_join
  - id: Ateam_flags_join
    in:
      - id: flagged_fraction_dict
        source:
          - concat/flagged_fraction_dict
      - id: filter_station
        default: ''
      - id: state
        default: 'concat'
    out:
      - id: flagged_fraction_antenna
    run: ../../steps/findRefAnt_join.cwl
    label: Ateam_flags_join
  - id: sort_times_into_freqGroups
    in:
      - id: msin
        source:
          - msin
      - id: numbands
        source: num_SBs_per_group
      - id: DP3fill
        default: true
      - id: stepname
        default: .dp3concat
      - id: firstSB
        source: reference_stationSB
      - id: truncateLastSBs
        default: false
    out:
      - id: filenames
      - id: groupnames
      - id: total_bandwidth
      - id: logfile
    run: ../../steps/sort_times_into_freqGroups.cwl
    label: sorttimesintofreqGroups
  - id: concat_logfiles_dp3concat
    in:
      - id: file_list
        source:
          - concat/dp3concat.log
      - id: file_prefix
        default: dp3concat
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_dp3concat
  - id: concat_logfiles_blsmooth
    in:
      - id: file_list
        source:
          - calibrate_target/BLsmooth.log
      - id: file_prefix
        default: blsmooth_target
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_blsmooth
  - id: concat_logfiles_calib
    in:
      - id: file_list
        source:
          - calibrate_target/gaincal.log
      - id: file_prefix
        default: gaincal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib
  - id: concat_logfiles_losoto
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - h5parm_collector/log
          - losoto_plot_P/logfile
          - losoto_plot_P2/logfile
          - losoto_plot_Pd/logfile
          - losoto_plot_Pd2/logfile
      - id: file_prefix
        default: losoto_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_losoto
  - id: concat_logfiles_unflagged
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - merge_array_files/output
          - plot_unflagged/logfile
      - id: file_prefix
        default: check_unflagged_fraction
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_unflagged
  - id: concat_logfiles_identify
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - identifybadantennas/logfile
          - identifybadantennas_join/logfile
      - id: file_prefix
        default: identifyBadAntennas
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_identify
  - id: concat_logfiles_RefAnt
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - findRefAnt_join/logfile
      - id: file_prefix
        default: findRefAnt
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_RefAnt
  - id: concat
    in:
      - id: msin
        source:
          - msin
      - id: group_id
        source: sort_times_into_freqGroups/groupnames
      - id: groups_specification
        source: sort_times_into_freqGroups/filenames
      - id: filter_baselines
        source: identifybadantennas_join/filter_out
      - id: avg_timeresolution_concat
        source: avg_timeresolution_concat
      - id: avg_freqresolution_concat
        source: avg_freqresolution_concat
    out:
      - id: msout
      - id: flagged_fraction_dict
      - id: dp3concat.log
    run: ./concat.cwl
    label: concat
    scatter:
      - group_id
  - id: aoflag
    in:
      - id: msin
        source: concat/msout
      - id: verbose
        default: true
      - id: concatenate-frequency
        default: true
      - id: strategy
        source: rfistrategy
      - id: reorder
        source: aoflag_reorder
      - id: chunk-size
        source: aoflag_chunksize
    out:
      - id: output_ms
      - id: logfile
    run: ../../steps/aoflag.cwl
    label: aoflag
  - id: check_unflagged_fraction
    in:
      - id: msin
        source: aoflag/output_ms
      - id: min_fraction
        source: min_unflagged_fraction
    out:
      - id: msout
      - id: frequency
      - id: unflagged_fraction
      - id: filenames
      - id: logfile
    run: ../../steps/check_unflagged_fraction.cwl
    label: check_unflagged_fraction
    scatter:
      - msin
  - id: merge_array
    in:
      - id: input
        source: check_unflagged_fraction/msout
    out:
      - id: output
    run: ../../steps/merge_array.cwl
    label: merge_array
  - id: merge_array_files
    in:
      - id: input
        source: check_unflagged_fraction/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files
  - id: check_filtered_MS_array
    in:
      - id: input
        source: merge_array/output
    out:
      - id: output
    run: ../../steps/check_filtered_MS_array.cwl
    label: check_filtered_MS_array
  - id: calibrate_target
    in:
      - id: msin
        source: check_filtered_MS_array/output
      - id: skymodel
        source: target_skymodel
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
    out:
      - id: msout
      - id: BLsmooth.log
      - id: gaincal.log
      - id: outh5parm
    run: ./calib_targ.cwl
    label: calibrate_target
    scatter:
      - msin
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - calibrate_target/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: H5parm_collector
  - id: plot_unflagged
    in:
      - id: frequencies
        source: check_unflagged_fraction/frequency
      - id: unflagged_fraction
        source: check_unflagged_fraction/unflagged_fraction
    out:
      - id: output_imag
      - id: logfile
    run: ../../steps/plot_unflagged.cwl
    label: plot_unflagged
  - id: losoto_plot_P
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: findRefAnt_join/refant
      - id: prefix
        default: ph_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_P
  - id: losoto_plot_P2
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: findRefAnt_join/refant
      - id: prefix
        default: ph_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_P2
  - id: losoto_plot_Pd
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: findRefAnt_join/refant
      - id: prefix
        default: ph_poldif
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd
  - id: losoto_plot_Pd2
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: findRefAnt_join/refant
      - id: prefix
        default: ph_poldif_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd2
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
