class: Workflow
cwlVersion: v1.2
id: concat
label: concat
inputs:
  - id: msin
    type: Directory[]
  - id: group_id
    type: string
  - id: groups_specification
    type: File
  - id: filter_baselines
    type: string?
    default: '*&'
  - id: avg_timeresolution_concat
    type: int?
    default: 8
  - id: avg_freqresolution_concat
    type: string?
    default: '97.64kHz'
outputs:
  - id: flagged_fraction_dict
    outputSource:
      - dp3concat/flagged_fraction_dict
    type: string
  - id: msout
    outputSource:
      - dp3concat/msout
    type: Directory
  - id: dp3concat.log
    outputSource:
      - concat_logfiles_dp3concat/output
    type: File
steps:
  - id: filter_ms_group
    in:
      - id: group_id
        source: group_id
      - id: groups_specification
        source: groups_specification
      - id: measurement_sets
        source:
          - msin
    out:
      - id: output
      - id: selected_ms
    run: ../../steps/filter_ms_group.cwl
    label: filter_ms_group
  - id: dp3concat
    in:
      - id: msin
        source:
          - msin
      - id: msin_fname
        source: filter_ms_group/selected_ms
      - id: msout_name
        source: group_id
      - id: msin_datacolumn
        default: DATA
      - id: msout_datacolumn
        default: DATA
      - id: filter_baselines
        source: filter_baselines
      - id: filter_remove
        default: true
      - id: overwrite
        default: false
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: missingdata
        default: true
      - id: baseline
        source: filter_baselines
      - id: avg_timeresolution
        source: avg_timeresolution_concat
      - id: avg_freqresolution
        source: avg_freqresolution_concat
    out:
      - id: msout
      - id: flagged_fraction_dict
      - id: logfile
    run: ../../steps/dp3concat.cwl
    label: dp3concat
  - id: concat_logfiles_dp3concat
    in:
      - id: file_list
        source:
          - dp3concat/logfile
      - id: file_prefix
        default: dp3concat
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_dp3concat

requirements: []
