class: Workflow
cwlVersion: v1.2
id: prep
label: prep
inputs:
  - id: msin
    type: Directory[]
  - id: cal_solutions
    type: File
  - id: flag_baselines
    type: string[]?
    default: []
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
  - id: raw_data
    type: boolean?
    default: false
  - id: clip_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: clipAteam
    type: boolean?
    default: true
  - id: demix_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: demix
    type: boolean?
  - id: apply_tec
    type: boolean?
    default: false
  - id: apply_clock
    type: boolean?
    default: true
  - id: apply_phase
    type: boolean?
    default: false
  - id: apply_RM
    type: boolean?
    default: true
  - id: apply_beam
    type: boolean?
    default: true
  - id: updateweights
    type: boolean?
    default: true
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: memoryperc
    type: int?
    default: 20
  - id: min_separation
    type: int?
    default: 30
  - id: A-Team_skymodel
    type: File?
  - id: avg_timeresolution
    type: int?
    default: 4
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
  - id: ionex_server
    type: string?
    default: 'http://ftp.aiub.unibe.ch/CODE/'
  - id: ionex_prefix
    type: string?
    default: CODG
  - id: proxy_server
    type: string?
    default: null
  - id: proxy_port
    type: int?
    default: null
  - id: proxy_type
    type: string?
    default: null
  - id: proxy_user
    type: string?
    default: null
  - id: proxy_pass
    type: string?
    default: null
  - id: elevation
    type: string?
    default: 0deg..15deg
  - id: amplmin
    type: float?
    default: 1.e-30
  - id: target_skymodel
    type: File?
  - id: skymodel_source
    type: string?
    default: 'TGSS'
  - id: use_target
    type: boolean?
    default: true
  - id: targetname
    type: string?
    default: 'pointing'
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
outputs:
  - id: compare_stations_filter
    outputSource:
      - compare_station_list/filter_out
    type: string
  - id: outh5parm
    outputSource:
      - createRMh5parm/h5parmout
    type: File
  - id: inspection
    outputSource:
      - check_ateam_separation/output_imag
      - losoto_plot_RM/output_plots
      - plot_Ateamclipper/output_imag
    type: File[]
    linkMerge: merge_flattened
    pickValue: all_non_null
  - id: msout
    outputSource:
      - dp3_prep_target/msout
    type: Directory[]
  - id: check_Ateam_separation.json
    outputSource:
      - check_ateam_separation/output_json
    type: File
  - id: logfiles
    outputSource:
      - find_skymodel_target/logfile
      - concat_logfiles_skymodels/output
      - check_ateam_separation/logfile
      - check_demix/logfile
      - concat_logfiles_stationlist/output
      - concat_logfiles_RMextract/output
      - concat_logfiles_prep_targ/output
      - concat_logfiles_predict_targ/output
      - concat_logfiles_clipper_targ/output
    type: File[]
    linkMerge: merge_flattened
    pickValue: all_non_null
  - id: initial_flags_join_out
    outputSource:
      - initial_flags_join/flagged_fraction_antenna
    type: File
  - id: prep_flags_join_out
    outputSource:
      - prep_flags_join/flagged_fraction_antenna
    type: File
  - id: target_sourcedb
    outputSource:
      - find_skymodel_target/skymodel
    type:
      - File
      - Directory
steps:
  - id: find_skymodel_target
    in:
      - id: msin
        source:
          - msin
      - id: SkymodelPath
        source: target_skymodel
      - id: Radius
        default: 5
      - id: Source
        source: skymodel_source
      - id: DoDownload
        source: use_target
      - id: targetname
        source: targetname
    out:
      - id: skymodel
      - id: logfile
    run: ../../steps/find_skymodel_target.cwl
    label: find_skymodel_target
  - id: merge_skymodels
    in:
      - id: inmodel1
        source: find_skymodel_target/skymodel
      - id: inmodel2
        source: A-Team_skymodel
    out:
      - id: skymodel_out
      - id: logfile
    run: ../../steps/merge_skymodels.cwl
    label: merge_skymodels
  - id: initial_flags_join
    in:
      - id: flagged_fraction_dict
        source:
          - dp3_prep_target/initial_flags_out
      - id: filter_station
        default: ''
      - id: state
        default: initial
    out:
      - id: flagged_fraction_antenna
      - id: logfile
    run: ./../../steps/findRefAnt_join.cwl
    label: initial_flags_join
  - id: prep_flags_join
    in:
      - id: flagged_fraction_dict
        source:
          - dp3_prep_target/prep_flags_out
      - id: filter_station
        default: ''
      - id: state
        default: prep
    out:
      - id: flagged_fraction_antenna
      - id: logfile
    run: ./../../steps/findRefAnt_join.cwl
    label: prep_flags_join
  - id: check_ateam_separation
    in:
      - id: ms
        source:
          - msin
      - id: min_separation
        source: min_separation
    out:
      - id: output_imag
      - id: output_json
      - id: logfile
    run: ../../steps/check_ateam_separation.cwl
    label: check_Ateam_separation
  - id: check_demix
    in:
      - id: demix
        source: demix
      - id: skymodel
        source: A-Team_skymodel
      - id: demix_sources
        source: demix_sources
      - id: clip_sources
        source: clip_sources
      - id: Ateam_separation_file
        source: check_ateam_separation/output_json
    out:
      - id: out_demix
      - id: out_demix_sources
      - id: out_clip_sources
      - id: logfile
    run: ../../steps/check_demix.cwl
    label: check_demix
  - id: compare_station_list
    in:
      - id: msin
        source:
          - msin
      - id: h5parmdb
        source: cal_solutions
      - id: solset_name
        default: calibrator
      - id: filter
        source: filter_baselines
    out:
      - id: filter_out
      - id: logfile
    run: ../../steps/compare_station_list.cwl
    label: compare_station_list
  - id: createRMh5parm
    in:
      - id: msin
        source:
          - msin
      - id: h5parm
        source: cal_solutions
      - id: ionex_server
        source: ionex_server
      - id: ionex_prefix
        source: ionex_prefix
      - id: solset
        default: target
      - id: proxyserver
        source: proxy_server
      - id: proxyport
        source: proxy_port
      - id: proxytype
        source: proxy_type
      - id: proxyuser
        source: proxy_user
      - id: proxypass
        source: proxy_pass
    out:
      - id: h5parmout
      - id: logfile
    run: ../../steps/createRMh5parm.cwl
    label: createRMh5parm
  - id: losoto_plot_RM
    in:
      - id: input_h5parm
        source: createRMh5parm/h5parmout
      - id: soltab
        default: target/RMextract
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: prefix
        default: RMextract
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_RM
  - id: dp3_prep_target
    in:
      - id: baselines_to_flag
        source:
          - flag_baselines
      - id: elevation_to_flag
        source: elevation
      - id: min_amplitude_to_flag
        source: amplmin
      - id: memoryperc
        source: memoryperc
      - id: raw_data
        source: raw_data
      - id: demix
        source: check_demix/out_demix
      - id: msin
        linkMerge: merge_flattened
        source:
          - msin
      - id: skymodel
        source: merge_skymodels/skymodel_out
      - id: timeresolution
        source: avg_timeresolution
      - id: freqresolution
        source: avg_freqresolution
      - id: demix_timeres
        source: demix_timeres
      - id: demix_freqres
        source: demix_freqres
      - id: process_baselines_target
        source: process_baselines_target
      - id: ntimechunk
        source: max_dp3_threads
      - id: subtract_sources
        source: check_demix/out_demix_sources
      - id: parmdb
        source: createRMh5parm/h5parmout
      - id: apply_tec_correction
        source: apply_tec
      - id: apply_rm_correction
        source: apply_RM
      - id: apply_phase_correction
        source: apply_phase
      - id: apply_clock_correction
        source: apply_clock
      - id: apply_beam_correction
        source: apply_beam
      - id: filter_baselines
        source: compare_station_list/filter_out
      - id: updateweights
        source: updateweights
      - id: clip_sources
        source: check_demix/out_clip_sources
      - id: clipAteam
        source: clipAteam
      - id: target_source
        source: targetname
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
    out:
      - id: prep_flags_out
      - id: initial_flags_out
      - id: prep_logfile
      - id: predict_logfile
      - id: clipper_logfile
      - id: msout
      - id: clipper_output
    run: ./dp3_prep_targ.cwl
    label: dp3_prep_target
    scatter:
      - msin
  - id: plot_Ateamclipper
    in:
      - id: clipper_output
        source: concat_logfiles_clipper_output/output
      - id: execute
        source: clipAteam
    out:
      - id: output_imag
    run: ../../steps/plot_Ateamclipper.cwl
    when: $(inputs.execute)
    label: concat_logfiles_clipper_output
  - id: concat_logfiles_clipper_output
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - dp3_prep_target/clipper_output
      - id: file_prefix
        default: Ateamclipper
      - id: file_suffix
        default: txt
      - id: execute
        source: clipAteam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_clipper_output
  - id: concat_logfiles_prep_targ
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - dp3_prep_target/prep_logfile
      - id: file_prefix
        default: dp3_prep_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_prep_target
  - id: concat_logfiles_predict_targ
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - dp3_prep_target/predict_logfile
      - id: file_prefix
        default: predict_targ
      - id: execute
        source: clipAteam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_predict_targ
  - id: concat_logfiles_clipper_targ
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - dp3_prep_target/clipper_logfile
        pickValue: all_non_null
      - id: file_prefix
        default: Ateamclipper
      - id: execute
        source: clipAteam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_clipper_targ
  - id: concat_logfiles_RMextract
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - createRMh5parm/logfile
          - losoto_plot_RM/logfile
      - id: file_prefix
        default: RMextract
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_RMextract
  - id: concat_logfiles_stationlist
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - compare_station_list/logfile
      - id: file_prefix
        default: compareStationList
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_stationlist
  - id: concat_logfiles_skymodels
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - merge_skymodels/logfile
      - id: file_prefix
        default: merge_skymodels
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_skymodels
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement