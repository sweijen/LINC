class: Workflow
cwlVersion: v1.2
id: finalize
label: finalize
inputs:
  - id: refant
    type: string?
    default: 'CS001HBA0'
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
  - id: removed_bands
    type: string[]?
    default: []
  - id: demix_sources
    type: string[]?
    default: []
  - id: demix
    type: boolean?
    default: false
  - id: clip_sources
    type: string[]?
    default: []
  - id: msin
    type: Directory[]
  - id: input_h5parm
    type: File
  - id: inh5parm_logfile
    type: File
  - id: gsmcal_step
    type: string?
    default: 'phase'
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    default: '[CR]S*&'
  - id: insolutions
    type: File
  - id: compression_bitrate
    type: int?
    default: 16
  - id: skymodel_source
    type: string?
    default: 'TGSS'
  - id: total_bandwidth
    type: int?
    default: 50
  - id: check_Ateam_separation.json
    type: File
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
  - id: compare_stations_filter
    type: string?
    default: '[CR]S*&'
  - id: flags
    type: File[]
  - id: targetname
    type: string?
    default: 'pointing'
outputs:
  - id: msout
    outputSource:
      - apply_gsmcal/msout
    type: Directory[]
  - id: solutions
    outputSource:
      - h5parm_pointingname/outh5parm
    type: File
  - id: logfiles
    outputSource:
      - concat_logfiles_applygsm/output
      - concat_logfiles_solutions/output
      - concat_logfiles_structure/output
      - concat_logfiles_wsclean/output
      - concat_logfiles_summary/output
      - concat_logfiles_uvplot/output
    type: File[]
    linkMerge: merge_flattened
  - id: inspection
    outputSource:
      - structure_function/output_plot
      - wsclean/image
      - uvplot/output_image
    type: File[]
    linkMerge: merge_flattened
  - id: summary_file
    outputSource:
      - summary/summary_file
    type: File
steps:
  - id: add_missing_stations
    in:
      - id: h5parm
        source: input_h5parm
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        source: gsmcal_step
        valueFrom: $(self+'000')
      - id: soltab_out
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    label: add_missing_stations
  - id: apply_gsmcal
    in:
      - id: msin
        source: msin
      - id: msout_name
        linkMerge: merge_flattened
        source:
          - msin
        valueFrom: $(self.nameroot+'_pre-cal.ms')
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: write_solutions/outh5parm
      - id: msout_datacolumn
        default: DATA
      - id: correction
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: solset
        default: target
      - id: storagemanager
        default: Dysco
      - id: databitrate
        source: compression_bitrate
    out:
      - id: msout
      - id: flagged_fraction_dict
      - id: logfile
    run: ../../steps/applytarget.cwl
    label: apply_gsmcal
    scatter:
      - msin
      - msout_name
    scatterMethod: dotproduct
  - id: final_flags_join
    in:
      - id: flagged_fraction_dict
        source:
          - apply_gsmcal/flagged_fraction_dict
      - id: filter_station
        default: ''
      - id: state
        default: 'final'
    out:
      - id: flagged_fraction_antenna
    run: ./../../steps/findRefAnt_join.cwl
    label: final_flags_join
  - id: average
    in:
      - id: msin
        source: apply_gsmcal/msout
      - id: msout_name
        linkMerge: merge_flattened
        source:
          - apply_gsmcal/msout
        valueFrom: $(self.nameroot+'_wsclean.ms')
      - id: msin_datacolumn
        default: DATA
      - id: overwrite
        default: false
      - id: storagemanager
        default: Dysco
      - id: avg_timestep
        default: 2
      - id: avg_freqstep
        default: 2
    out:
      - id: msout
      - id: logfile
    run: ../../steps/average.cwl
    label: average
    scatter:
      - msin
      - msout_name
    scatterMethod: dotproduct
  - id: summary
    in:
      - id: flagFiles
        source: 
          - flags
          - final_flags_join/flagged_fraction_antenna
        linkMerge: merge_flattened
      - id: pipeline
        default: 'LINC'
      - id: run_type
        default: 'target'
      - id: filter
        source: filter_baselines
      - id: bad_antennas
        source:
          - bad_antennas
          - compare_stations_filter
        valueFrom: $(self.join(''))
      - id: Ateam_separation_file
        source: check_Ateam_separation.json
      - id: solutions
        source: h5parm_pointingname/outh5parm
      - id: clip_sources
        source: clip_sources
        valueFrom: "$(self.join(','))"
      - id: demix
        source: demix
        valueFrom: '$(self ? "True" : "False")'
      - id: demix_sources
        source: demix_sources
        valueFrom: "$(self.join(','))"
      - id: removed_bands
        source: removed_bands
        valueFrom: "$(self.join(','))"
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: refant
        source: refant
    out:
      - id: summary_file
      - id: logfile
    run: ../../steps/summary.cwl
    label: summary
  - id: uvplot
    in:
      - id: MSfiles
        source: apply_gsmcal/msout
      - id: output_name
        source: targetname
        valueFrom: $(self)_uv-coverage.png
      - id: title
        source: targetname
        valueFrom: '"uv coverage of the target pointing: $(self)"'
      - id: wideband
        default: true
    out:
      - id: output_image
      - id: logfile
    run: ../../steps/uvplot.cwl
    label: uvplot
  - id: wsclean
    in:
      - id: msin
        source:
          - average/msout
      - id: image_scale
        default: 15asec
      - id: auto_threshold
        default: 5
      - id: image_size
        default: [2500,2500]
      - id: niter
        default: 10000
      - id: nmiter
        default: 5
      - id: multiscale
        default: true
      - id: use-wgridder
        default: true
      - id: mgain
        default: 0.8
      - id: parallel-deconvolution
        default: 1500
      - id: parallel-reordering
        default: 4
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/1e7) < 1 ? 1 : Math.round(self/1e7))'
      - id: join-channels
        source: total_bandwidth
        valueFrom: $(Math.round(self/1e7) > 1)
      - id: deconvolution-channels
        source: total_bandwidth
        valueFrom: '$(Math.round(self/1e7) > 1 ? (Math.round(self/1e7) > 3 ? 3 : Math.round(self/1e7)) : false )'
      - id: fit-spectral-pol
        source: total_bandwidth
        valueFrom: '$(Math.round(self/1e7) > 1 ? (Math.round(self/1e7) > 3 ? 3 : Math.round(self/1e7)) : false )'
      - id: taper-gaussian
        default: 40asec
      - id: weighting
        default: briggs -0.5
      - id: maxuvw-m
        default: 20000
      - id: tempdir
        default: '/tmp/'
      - id: image_name
        source: targetname
    out:
      - id: dirty_image
      - id: image
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: wsclean
  - id: merge_array_files
    in:
      - id: input
        source:
          - apply_gsmcal/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files
  - id: write_solutions
    in:
      - id: h5parmFile
        source: add_missing_stations/outh5parm
      - id: outsolset
        default: target
      - id: insoltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
  - id: h5parm_pointingname
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solsetName
        default: target
      - id: pointing
        source: targetname
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parm_pointingname.cwl
    label: h5parm_pointingname
  - id: structure_function
    in:
      - id: input_h5parm
        source: write_solutions/outh5parm
      - id: soltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $('target/'+self.join(''))
      - id: plotName
        source: targetname
        valueFrom: $(self+'_structure.png')
    out:
      - id: output_plot
      - id: logfile
    run: ../../steps/LoSoTo.Structure.cwl
    label: structure_function
  - id: concat_logfiles_applygsm
    in:
      - id: file_list
        source:
          - merge_array_files/output
      - id: file_prefix
        default: apply_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applygsm
  - id: concat_logfiles_solutions
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - add_missing_stations/log
          - write_solutions/log
          - h5parm_pointingname/log
      - id: file_prefix
        default: losoto_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_solutions
  - id: concat_logfiles_structure
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - structure_function/logfile
      - id: file_prefix
        source: targetname
        valueFrom: $(self+'_structure')
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_structure
  - id: concat_logfiles_wsclean
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - wsclean/logfile
      - id: file_prefix
        default: wsclean
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_wsclean
  - id: concat_logfiles_summary
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - summary/logfile
      - id: file_prefix
        source: targetname
        valueFrom: $(self+'_summary')
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_summary
  - id: concat_logfiles_uvplot
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - uvplot/logfile
      - id: file_prefix
        default: uvplot
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_uvplot
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
