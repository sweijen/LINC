class: Workflow
cwlVersion: v1.2
id: calibrate_target
label: calibrate_target
inputs:
  - id: msin
    type: Directory
  - id: skymodel
    type:
      - File
      - Directory
  - id: do_smooth
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
outputs:
  - id: msout
    outputSource:
      - calib_targ/msout
    type: Directory
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
  - id: gaincal.log
    outputSource:
      - concat_logfiles_gaincal/output
    type: File
  - id: outh5parm
    outputSource:
      - calib_targ/h5parm
    type: File
steps:
  - id: BLsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: DATA
      - id: restore
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: calib_targ
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msout_name
        default: '.'
      - id: blrange
        default:
          - 150
          - 9999999
      - id: caltype
        default: phaseonly
      - id: sourcedb
        source: skymodel
      - id: maxiter
        default: 50
      - id: solint
        default: 1
      - id: nchan
        default: 0
      - id: tolerance
        default: 1e-3
      - id: propagatesolutions
        source: propagatesolutions
      - id: usebeammodel
        default: true
      - id: usechannelfreq
        default: true
      - id: beammode
        default: array_factor
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/gaincal.cwl
  - id: concat_logfiles_gaincal
    in:
      - id: file_list
        source:
          - calib_targ/logfile
      - id: file_prefix
        default: gaincal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_gaincal
requirements:
  - class: MultipleInputFeatureRequirement
