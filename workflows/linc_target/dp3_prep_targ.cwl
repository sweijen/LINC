class: Workflow
cwlVersion: v1.2
id: dp3_prep_targ
label: dp3_prep_targ
inputs:
  - id: baselines_to_flag
    type: string[]?
    default: []
  - id: elevation_to_flag
    type: string?
    default: '0deg..15deg'
  - id: min_amplitude_to_flag
    type: float?
    default: 1e-30
  - id: memoryperc
    type: int?
    default: 20
  - id: raw_data
    type: boolean?
    default: false
  - id: demix
    type: boolean?
    default: false
  - id: msin
    type: Directory
  - id: skymodel
    type:
      - File
      - Directory
  - id: timeresolution
    type: int?
    default: 4
  - id: freqresolution
    type: string?
    default: '48.82kHz'
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: target_source
    type: string?
    default: 'pointing'
  - id: ntimechunk
    type: int?
    default: 16
  - id: subtract_sources
    type: string[]?
    default: []
  - id: parmdb
    type: File
  - id: apply_tec_correction
    type: boolean?
    default: false
  - id: apply_rm_correction
    type: boolean?
    default: true
  - id: apply_phase_correction
    type: boolean?
    default: false
  - id: apply_clock_correction
    type: boolean?
    default: true
  - id: apply_beam_correction
    type: boolean?
    default: true
  - id: filter_baselines
    type: string?
    default: ''
  - id: updateweights
    type: boolean?
    default: true
  - id: clip_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: clipAteam
    type: boolean?
    default: true
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
outputs:
  - id: initial_flags_out
    outputSource:
      - dp3_execute/flagged_fraction_dict_initial
    type: string
  - id: prep_flags_out
    outputSource:
      - dp3_execute/flagged_fraction_dict_prep
    type: string
  - id: prep_logfile
    outputSource:
      - concat_logfiles_dp3/output
    type: File
  - id: predict_logfile
    outputSource:
      - concat_logfiles_predict/output
    type: File
    pickValue: all_non_null
  - id: clipper_logfile
    outputSource:
      - concat_logfiles_clipper/output
    type: File
    pickValue: all_non_null
  - id: msout
    outputSource:
      - Ateamclipper/msout
      - dp3_execute/msout
    type: Directory
    pickValue: first_non_null
  - id: clipper_output
    outputSource:
      - Ateamclipper/output
    type: File
    pickValue: all_non_null
steps:
  - id: define_parset
    in:
      - id: raw_data
        source: raw_data
      - id: demix
        source: demix
      - id: apply_tec_correction
        source: apply_tec_correction
      - id: apply_rm_correction
        source: apply_rm_correction
      - id: apply_phase_correction
        source: apply_phase_correction
      - id: apply_clock_correction
        source: apply_clock_correction
      - id: apply_beam_correction
        source: apply_beam_correction
      - id: filter_baselines
        source: filter_baselines
      - id: memoryperc
        source: memoryperc
      - id: baselines_to_flag
        source:
          - baselines_to_flag
      - id: elevation_to_flag
        source: elevation_to_flag
      - id: min_amplitude_to_flag
        source: min_amplitude_to_flag
      - id: updateweights
        source: updateweights
      - id: timeresolution
        source: timeresolution
      - id: freqresolution
        source: freqresolution
      - id: process_baselines_target
        source: process_baselines_target
      - id: demix_timeres
        source: demix_timeres
      - id: demix_freqres
        source: demix_freqres
      - id: target_source
        source: target_source
      - id: subtract_sources
        source:
          - subtract_sources
      - id: ntimechunk
        source: ntimechunk
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
    out:
      - id: output
    run: ../../steps/dp3_make_parset_target.cwl
    label: make_parset
  - id: concat_logfiles_dp3
    in:
      - id: file_list
        source:
          - dp3_execute/logfile
      - id: file_prefix
        default: dp3
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_dp3
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - predict/logfile
        pickValue: all_non_null  
      - id: file_prefix
        default: predict_targ
      - id: execute
        source: clipAteam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_predict
  - id: concat_logfiles_clipper
    in:
      - id: file_list
        source:
          - Ateamclipper/logfile
        pickValue: all_non_null
      - id: file_prefix
        default: Ateamclipper
      - id: execute
        source: clipAteam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_clipper
  - id: dp3_execute
    in:
      - id: parset
        source: define_parset/output
      - id: msin
        source: msin
      - id: solutions
        source: parmdb
      - id: msout_name
        source: msin
        valueFrom: $(self.basename)
      - id: autoweight
        source: raw_data
      - id: baseline
        source: filter_baselines
      - id: output_column
        default: DATA
      - id: input_column
        default: DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: skymodel
        source: skymodel
    out:
      - id: msout
      - id: flagged_fraction_dict_initial
      - id: flagged_fraction_dict_prep
      - id: logfile
    run: ../../steps/dp3_prep_target.cwl
    label: DP3.Execute
  - id: predict
    in:
      - id: msin
        source: dp3_execute/msout
      - id: msin_datacolumn
        default: DATA
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: sources_db
        source: skymodel
      - id: sources
        source:
          - clip_sources
      - id: usebeammodel
        default: true
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: filter_baselines
        source: process_baselines_target
      - id: execute
        source: clipAteam
    out:
      - id: msout
      - id: logfile
    run: ../../steps/filter_predict.cwl
    when: $(inputs.execute)
  - id: Ateamclipper
    in:
      - id: msin
        source:
          - predict/msout
        pickValue: all_non_null
      - id: execute
        source: clipAteam
    out:
      - id: msout
      - id: logfile
      - id: output
    run: ../../steps/Ateamclipper.cwl
    when: $(inputs.execute)
    label: Ateamclipper
requirements:
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
  - class: SubworkflowFeatureRequirement
