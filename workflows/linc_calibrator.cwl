class: Workflow
cwlVersion: v1.2
id: linc_calibrator
label: linc_calibrator
inputs:
  - id: msin
    type: Directory[]
  - id: refant
    type: string?
    default: 'CS00.*'
  - id: flag_baselines
    type: string[]?
    default: []
  - id: process_baselines_cal
    type: string?
    default: '*&'
  - id: filter_baselines
    type: string?
    default: '*&'
  - id: fit_offset_PA
    type: boolean?
    default: false
  - id: do_smooth
    type: boolean?
    default: false
  - id: rfistrategy
    type:
      - File?
      - string?
  - id: max2interpolate
    type: int?
    default: 30
  - id: ampRange
    type: float[]?
    default:
      - 0
      - 0
  - id: skip_international
    type: boolean?
    default: true
  - id: raw_data
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: flagunconverged
    type: boolean?
    default: false
  - id: maxStddev
    type: float?
    default: -1
  - id: solutions2transfer
    type: File?
  - id: antennas2transfer
    type: string?
    default: '[FUSPID].*'
  - id: do_transfer
    type: boolean?
    default: false
  - id: trusted_sources
    type: string
    default: '3C48,3C147,3C196,3C295'
  - id: demix_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: demix
    type: boolean?
  - id: ion_3rd
    type: boolean?
    default: false
  - id: clock_smooth
    type: boolean?
    default: true
  - id: tables2export
    type: string?
    default: clock
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: memoryperc
    type: int?
    default: 20
  - id: min_separation
    type: int?
    default: 30
  - id: max_separation_arcmin
    type: float?
    default: 1
  - id: calibrator_path_skymodel
    type:
      - Directory?
      - File?
  - id: A-Team_skymodel
    type: File?
  - id: avg_timeresolution
    type: int?
    default: 4
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
  - id: bandpass_freqresolution
    type: string?
    default: 195.3125kHz
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
outputs:
  - id: inspection
    linkMerge: merge_flattened
    outputSource:
      - prep/check_Ateam_separation.png
      - pa/inspection
      - fr/inspection
      - bp/inspection
      - ion/inspection
    type: File[]
  - id: solutions
    outputSource:
      - ion/outsolutions
    type: File
  - id: logfiles
    outputSource:
      - prep/logfiles
      - concat_logfiles_RefAnt/output
      - pa/logfiles
      - fr/logfiles
      - bp/logfiles
      - ion/logfiles
    type: File[]
    linkMerge: merge_flattened
  - id: summary_file
    outputSource:
      - ion/summary_file
    type: File
steps:
  - id: initial_flags_join
    in:
      - id: flagged_fraction_dict
        source:
          - prep/flagged_fraction_dict
      - id: filter_station
        default: ''
      - id: state
        default: 'initial'
    out:
      - id: flagged_fraction_antenna
    run: ./../steps/findRefAnt_join.cwl
    label: initial_flags_join
  - id: prep
    in:
      - id: msin
        source:
          - msin
      - id: filter_baselines
        source: filter_baselines
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: demix
        source: demix
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: memoryperc
        source: memoryperc
      - id: flag_baselines
        source:
          - flag_baselines
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: process_baselines_cal
        source: process_baselines_cal
      - id: demix_timeres
        source: demix_timeres
      - id: demix_freqres
        source: demix_freqres
      - id: demix_sources
        source:
          - demix_sources
      - id: min_separation
        source: min_separation
      - id: do_smooth
        source: do_smooth
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: calibrator_path_skymodel
        source: calibrator_path_skymodel
      - id: rfistrategy
        source: rfistrategy
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
      - id: aoflag_reorder
        source: aoflag_reorder
      - id: aoflag_chunksize
        source: aoflag_chunksize
    out:
      - id: outh5parm
      - id: logfiles
      - id: outh5parm_logfile
      - id: check_Ateam_separation.png
      - id: check_Ateam_separation.json
      - id: flagged_fraction_dict_refant
      - id: flagged_fraction_dict
      - id: msout
      - id: calibrator_name
    run: ./linc_calibrator/prep.cwl
    label: prep
  - id: findRefAnt_join
    in:
      - id: flagged_fraction_dict
        source: prep/flagged_fraction_dict_refant
      - id: filter_station
        source: refant
    out:
      - id: refant
      - id: logfile
    run: ./../steps/findRefAnt_join.cwl
    label: findRefAnt_join
  - id: concat_logfiles_RefAnt
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - findRefAnt_join/logfile
      - id: file_prefix
        default: findRefAnt
    out:
      - id: output
    run: ./../steps/concatenate_files.cwl
    label: concat_logfiles_RefAnt
  - id: pa
    in:
      - id: msin
        source:
          - prep/msout
      - id: h5parm
        source:
          - prep/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - prep/outh5parm_logfile
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: do_smooth
        source: do_smooth
      - id: fit_offset_PA
        source: fit_offset_PA
    out:
      - id: msout
      - id: outsolutions
      - id: inspection
      - id: logfiles
      - id: outh5parm
      - id: outh5parm_logfile
    run: ./linc_calibrator/pa.cwl
    label: PA
  - id: fr
    in:
      - id: msin
        source:
          - pa/msout
      - id: h5parm
        source:
          - pa/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - pa/outh5parm_logfile
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: do_smooth
        source: do_smooth
      - id: insolutions
        source: pa/outsolutions
    out:
      - id: msout
      - id: outsolutions
      - id: inspection
      - id: logfiles
      - id: outh5parm
      - id: outh5parm_logfile
    run: ./linc_calibrator/fr.cwl
    label: FR
  - id: bp
    in:
      - id: msin
        source:
          - fr/msout
      - id: h5parm
        source:
          - fr/outh5parm
      - id: inh5parm_logfile
        source:
          - fr/outh5parm_logfile
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: ampRange
        source: ampRange
      - id: skipInternational
        source: skip_international
      - id: max2interpolate
        source: max2interpolate
      - id: bandpass_freqresolution
        source: bandpass_freqresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: do_smooth
        source: do_smooth
      - id: insolutions
        source: fr/outsolutions
      - id: solutions2transfer
        source: solutions2transfer
      - id: antennas2transfer
        source: antennas2transfer
      - id: do_transfer
        source: do_transfer
      - id: trusted_sources
        source: trusted_sources
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: calibrator_name
        source: prep/calibrator_name
    out:
      - id: outsolutions
      - id: inspection
      - id: logfiles
      - id: outh5parm
      - id: outh5parm_logfile
      - id: final_flags_join_out
    run: ./linc_calibrator/bp.cwl
    label: BP
  - id: ion
    in:
      - id: h5parm
        source:
          - bp/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - bp/outh5parm_logfile
      - id: insolutions
        source: bp/outsolutions
      - id: maxStddev
        source: maxStddev
      - id: ion_3rd
        source: ion_3rd
      - id: tables2export
        source: tables2export
      - id: clock_smooth
        source: clock_smooth
      - id: calibrator_name
        source: prep/calibrator_name
      - id: flags
        linkMerge: merge_flattened
        source:
          - initial_flags_join/flagged_fraction_antenna
          - bp/final_flags_join_out
      - id: demix
        source: demix
      - id: demix_sources
        source: demix_sources
      - id: filter_baselines
        source: filter_baselines
      - id: check_Ateam_separation.json
        source: prep/check_Ateam_separation.json
    out:
      - id: summary_file
      - id: outsolutions
      - id: inspection
      - id: logfiles
    run: ./linc_calibrator/ion.cwl
    label: ion
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
