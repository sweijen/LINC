class: Workflow
cwlVersion: v1.2
id: HBA_calibrator
label: HBA_calibrator
inputs:
  - id: msin
    type: Directory[]
  - id: refant
    type: string?
    default: 'CS00.*'
  - id: flag_baselines
    type: string[]?
    default: []
  - id: process_baselines_cal
    type: string?
    default: '*&'
  - id: filter_baselines
    type: string?
    default: '*&'
  - id: fit_offset_PA
    type: boolean?
    default: false
  - id: do_smooth
    type: boolean?
    default: true
  - id: rfistrategy
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/rfistrategies/lofar-lba-wideband.lua'
  - id: max2interpolate
    type: int?
    default: 30
  - id: ampRange
    type: float[]?
    default:
      - 0
      - 0
  - id: skip_international
    type: boolean?
    default: true
  - id: raw_data
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: flagunconverged
    type: boolean?
    default: false
  - id: maxStddev
    type: float?
    default: -1.0
  - id: solutions2transfer
    type: File?
  - id: antennas2transfer
    type: string?
    default: '[FUSPID].*'
  - id: do_transfer
    type: boolean
    default: false
  - id: trusted_sources
    type: string
    default: '3C48,3C147,3C196,3C295,3C380'
  - id: demix_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: demix
    type: boolean?
  - id: ion_3rd
    type: boolean?
    default: true
  - id: clock_smooth
    type: boolean?
    default: false
  - id: tables2export
    type: string?
    default: phaseOrig
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: memoryperc
    type: int?
    default: 20
  - id: min_separation
    type: int?
    default: 30
  - id: max_separation_arcmin
    type: float?
    default: 1.0
  - id: calibrator_path_skymodel
    type: Directory?
  - id: A-Team_skymodel
    type: File?
  - id: avg_timeresolution
    type: int?
    default: 1
  - id: avg_freqresolution
    type: string?
    default: 24.41kHz
  - id: bandpass_freqresolution
    type: string?
    default: 195.3125kHz
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
outputs:
  - id: log_files
    outputSource:
      - save_logfiles/dir
    type: Directory
  - id: inspection_plots
    outputSource:
      - save_inspection/dir
    type: Directory
  - id: summary
    outputSource:
      - linc/summary_file
    type: File
  - id: solutions
    outputSource:
      - linc/solutions
    type: File
steps:
  - id: linc
    in:
      - id: msin
        source:
          - msin
      - id: refant
        source: refant
      - id: flag_baselines
        source:
          - flag_baselines
      - id: process_baselines_cal
        source: process_baselines_cal
      - id: filter_baselines
        source: filter_baselines
      - id: fit_offset_PA
        source: fit_offset_PA
      - id: do_smooth
        source: do_smooth
      - id: rfistrategy
        source: rfistrategy
      - id: max2interpolate
        source: max2interpolate
      - id: ampRange
        source:
          - ampRange
      - id: skip_international
        source: skip_international
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: maxStddev
        source: maxStddev
      - id: solutions2transfer
        source: solutions2transfer
      - id: antennas2transfer
        source: antennas2transfer
      - id: do_transfer
        source: do_transfer
      - id: trusted_sources
        source: trusted_sources
      - id: demix_sources
        source: demix_sources
      - id: demix_freqres
        source: demix_freqres
      - id: demix_timeres
        source: demix_timeres
      - id: demix
        source: demix
      - id: ion_3rd
        source: ion_3rd
      - id: clock_smooth
        source: clock_smooth
      - id: tables2export
        source: tables2export
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: memoryperc
        source: memoryperc
      - id: min_separation
        source: min_separation
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: calibrator_path_skymodel
        source: calibrator_path_skymodel
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: bandpass_freqresolution
        source: bandpass_freqresolution
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
      - id: aoflag_reorder
        source: aoflag_reorder
      - id: aoflag_chunksize
        source: aoflag_chunksize
    out:
      - id: logfiles
      - id: solutions
      - id: inspection
      - id: summary_file
    run: ./linc_calibrator.cwl
    label: linc_calibrator
  - id: save_logfiles
    in:
      - id: files
        linkMerge: merge_flattened
        source:
          - linc/logfiles
      - id: sub_directory_name
        default: logs
    out:
      - id: dir
    run: ./../steps/collectfiles.cwl
    label: save_logfiles
  - id: save_inspection
    in:
      - id: files
        linkMerge: merge_flattened
        source:
          - linc/inspection
      - id: sub_directory_name
        default: inspection
    out:
      - id: dir
    run: ./../steps/collectfiles.cwl
    label: save_inspection
requirements:
  - class: SubworkflowFeatureRequirement
  - class: MultipleInputFeatureRequirement
