#!/usr/bin/env python3
"""
Checks the outputs of a workflow run
"""
import argparse
from argparse import RawTextHelpFormatter
from filecmp import dircmp
import h5py
import sys
import numpy as np
import os


def check_all_files_present(dcmp, leftname='left', rightname='right'):
    """
    Checks recursively that all files are present in both compared directories

    Parameters
    ----------
    dcmp : dircmp object
        The result of dircmp(left, right)
    leftname : str, optional
        Name of the left part of the input dcmp
    rightname : str, optional
        Name of the right part of the input dcmp

    Returns
    -------
    agree : bool
        True if all files are present, False if not
    """
    agree = True
    if dcmp.left_only or dcmp.right_only:
        if dcmp.left_only:
            print('ERROR: The following files are present in the {0} but not in the '
                  '{1}: {2}'.format(leftname, rightname, dcmp.left_only))
        if dcmp.right_only:
            print('ERROR: The following files are present in the {0} but not in the '
                  '{1}: {2}'.format(rightname, leftname, dcmp.right_only))
        agree = False
    for sub_dcmp in dcmp.subdirs.values():
        if not check_all_files_present(sub_dcmp, leftname=leftname, rightname=rightname):
            agree = False
    return agree


def main(check, control):
    """
    Checks the outputs of an end-to-end workflow job

    Parameters
    ----------
    check : str
        Path to output results directory of job
    control : str
        Path to control results directory
    """
    error = False

    # Check that all expected output files are present
    dcmp = dircmp(check, control)
    all_present = check_all_files_present(dcmp, leftname='output', rightname='control')
    if not all_present:
        error = True

    check_path = os.path.join(check, 'cal_solutions.h5')
    control_path = os.path.join(control, 'cal_solutions.h5')
    check_h5 = h5py.File(check_path, 'r')
    control_h5 = h5py.File(control_path, 'r')

    # Check that the calibration solutions match the control ones for matching frequencies
    for solsetname in check_h5:
        soltabnames = [name for name in check_h5[solsetname]
                       if name not in ('source', 'antenna')]
        try:
            control_h5[solsetname]
        except KeyError:
            print("Error: solset {} not present in control.".format(solsetname))
            sys.exit(1)
        for soltabname in soltabnames:
            try:
                control_h5[solsetname][soltabname]
            except KeyError:
                print("Error: soltab {} not present in control.".format(soltabname))
                sys.exit(1)
            check_soltabval = check_h5[solsetname][soltabname]['val']
            control_soltabval = control_h5[solsetname][soltabname]['val']
            matching_vals = control_soltabval
            if 'freq' in check_h5[solsetname][soltabname].keys() and args.allow_frequency_subset:
                check_axes = check_soltabval.attrs['AXES'].decode('utf-8').split(',')
                freq_axis_index = check_axes.index('freq')
                check_soltabfreq = check_h5[solsetname][soltabname]['freq'][:]
                control_soltabfreq = control_h5[solsetname][soltabname]['freq'][:]
                matches = np.isclose(control_soltabfreq[:, np.newaxis], check_soltabfreq)
                matching_freq_indices = np.where(matches)[0]
                matching_vals = np.take(control_soltabval,
                                        matching_freq_indices,
                                        axis=freq_axis_index)
            if not np.allclose(check_soltabval, matching_vals,
                               rtol=1e-03, atol=1e-03, equal_nan=True):
                error = True
                print("ERROR: Val array of soltab {} does not match the control".format(soltabname))
                with open("check_soltab.{}.val".format(soltabname), "w") as f:
                    f.write(str(check_soltabval[:]))
                with open("control_soltab.{}.val".format(soltabname), "w") as f:
                    f.write(str(control_soltabval[:]))
            else:
                print("Val array of soltab {} matches the control".format(soltabname))
    check_h5.close()
    control_h5.close()
    sys.exit(error)


if __name__ == '__main__':
    descriptiontext = "Checks the output of a workflow run.\n"

    parser = argparse.ArgumentParser(description=descriptiontext, formatter_class=RawTextHelpFormatter)
    parser.add_argument('check_path', help='Path to output results')
    parser.add_argument('control_path', help='Path to control results')
    parser.add_argument('--allow-frequency-subset',
                        help='Use if frequency range in control differs from those in output',
                        action='store_true')
    args = parser.parse_args()

    main(args.check_path, args.control_path)
