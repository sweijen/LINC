# Some BBS skymodels

Here are some skymodel files, provided for your convenience. No claim is made about the completeness or correctness of the skymodels.

If you have additional skymodels for calibrators that might be of interest for others, then please add them to the wiki page at: http://www.lofar.org/wiki/doku.php?id=commissioning:flux_calibrators and drop me a note.

The files so far:
* *3C196-SH-offringa.skymodel* : 3C196 skymodel from Andre Offringa, rescaled to fit on the Scaife \& Heald flux scale
* *3c147-SH.skymodel* : 3C147 from Scaife \& Heald, single point source
* *3c286-SH.skymodel* : 3C286 from Scaife \& Heald, single point source
* *3c287-SH.skymodel* : 3C287 from Scaife \& Heald, single point source
* *3c295-twocomp.skymodel* : 3C295 with two components, flux from Scaife \& Heald
* *3C380_8h_SH.skymodel* : 3C380, from Scaife \& Heald, model set using high resolution LBA image 
* *3c48-SH.skymodel* : 3C48 from Scaife \& Heald, single point source
* *A-Team_4_Highres.skymodel* : High-resolution model of the A-Team sources
* *Ateamhighresdemix.sourcedb* : Pre-compiled version of the high-resolution model of the A-Team sources, e.g. for demixing.
* *A-Team_lowres.skymodel* : Low-resolution model of the A-Team sources
* *Calibrators_SH_lowres.skymodel* : All the calibrators from the Scaife \& Heald paper, single point sources.
